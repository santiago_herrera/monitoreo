[33mcommit a71f01328a065f5fa3ef708ed3772aae97dd8039[m[33m ([m[1;36mHEAD -> [m[1;32mdevelop[m[33m)[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Mon Jun 11 12:21:31 2018 -0500

    Se cambian nombres y se ponen eventos a botones

[33mcommit 49ef6d644cfca1e22202c6213546747db7df5ff6[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Mon Jun 4 20:39:45 2018 -0500

    inicio

[33mcommit 5fa8bccdf556f7ad0cd1399d86319283c6433205[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Mon Jun 4 20:38:53 2018 -0500

    Se agregan estadisticas con su tabla

[33mcommit 9218240a842941436dc12273e2026c7c4040308e[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Mon May 28 16:41:36 2018 -0500

    Se usa icono

[33mcommit 0ceff8f7982d4429dd665ece60d1968d8c245193[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Mon May 28 16:37:04 2018 -0500

    Se agrega el icono

[33mcommit 0542e5e9eac6991ab9654f603bd609c39cedd85d[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Mon May 28 16:27:45 2018 -0500

    Se agrega la vista del partido con un tablero generico

[33mcommit 4dc891da90fd976179ddf5aec1e4a09441a4eb45[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Sun May 27 12:10:38 2018 -0500

    Se agrega estadisticas

[33mcommit 5251731c208e6ae36144394385da3d0245be4a01[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Sun May 27 10:01:21 2018 -0500

    Se agrega jugadores a la lista de iniciar partido

[33mcommit 11fbe43b6d5769b1319addf62febb2d074d1f076[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Sat May 26 22:04:25 2018 -0500

    Se agrega lista de jugadores con db

[33mcommit 4ed6d424bbd532060abdf77e4b1012568936dcae[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Sat May 26 12:39:21 2018 -0500

    Se agregan actividades de estadisticas y jugadores

[33mcommit 6d6227beb5cbe1e2927671ba96a90aa9b4776a23[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Sat May 26 12:22:00 2018 -0500

    Se agrega modelo y actividad de inicio de partido

[33mcommit d7cb3e450aa29ea4fb0c87deca16d076f9b00b38[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Thu May 24 19:09:20 2018 -0500

    Se agrega logueo y registro

[33mcommit b9c1e85c4b7931d72c3f61dec8c5083498b31373[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Tue May 22 22:10:11 2018 -0500

    Se agrega primer version de logueo y registro

[33mcommit 68b2af849f18f1252a4e9df20e5e680673318640[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Tue May 22 21:06:00 2018 -0500

    Se agrega libreria para registro

[33mcommit c82d802165bbe142977f05acd857b09e17a8f41a[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Wed May 16 17:39:50 2018 -0500

    Commit inicial

[33mcommit 43e6d233488d068444fc05e9ae0c42a4a3bf1fe9[m
Author: Santiago Herrera <santiagoherrerav@gmail.com>
Date:   Wed May 16 17:39:37 2018 -0500

    Initial commit
