package co.edu.poli.monitoreo.actividades;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import co.edu.poli.monitoreo.R;

public class Ingresar extends AppCompatActivity {

    private final String TAG = this.getClass().getName();
    private FirebaseAuth mAuth;

    public ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        Intent intent = getIntent();
        final String correo = intent.getStringExtra("correo");

        TextView txtUsuario = findViewById(R.id.txtUsuario);
        Button btnpartidos = findViewById(R.id.btnPartido);
        Button btnJugadores = findViewById(R.id.btnJugadores);
        Button btnEstadisticas = findViewById(R.id.btnEstadisticas);

        btnpartidos.setOnClickListener(v -> {
            Intent intent1 = new Intent(getApplicationContext(), InicioPartido.class);
            intent1.putExtra("entrenador", correo);
            startActivity(intent1);
        });

        btnJugadores.setOnClickListener(v -> {
            Intent intent12 = new Intent(getApplicationContext(), Jugadores.class);
            intent12.putExtra("entrenador", correo);
            startActivity(intent12);
        });

        btnEstadisticas.setOnClickListener(v -> {
            Intent intent13 = new Intent(getApplicationContext(), Estadisticas.class);
            intent13.putExtra("entrenador", correo);
            startActivity(intent13);
        });

        txtUsuario.setText(correo);

        mAuth = FirebaseAuth.getInstance();

    }

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.ingresar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.cerrar:
                signOut();
                Intent intent1 = new Intent(getApplicationContext(), Inicio.class);
                startActivity(intent1);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void signOut() {
        mAuth.signOut();
    }
}
