package co.edu.poli.monitoreo.persistencia;

import android.provider.BaseColumns;

/**
 * Esquema de la base de datos para abogados
 */
public class Contratos {

    public static abstract class EntrenadorEntidad implements BaseColumns {
        public static final String TABLE_NAME = "entrenadores";

        public static final String ID = "id";
        public static final String NOMBRES = "nombres";
        public static final String APELLIDOS = "apellidos";
        public static final String CORREO = "correo";
    }

    public static abstract class JugadorEntidad implements BaseColumns {
        public static final String TABLE_NAME = "jugadores";

        public static final String ID = "id";
        public static final String NOMBRES = "nombres";
        public static final String APELLIDOS = "apellidos";
        public static final String CORREO = "correo";
        public static final String EDAD = "edad";
        public static final String CATEGORIA = "categoria";
        public static final String ENTRENADOR = "entrenador";
    }

    public static abstract class PartidoEntidad implements BaseColumns {
        public static final String TABLE_NAME = "partidos";

        public static final String ID = "id";
        public static final String ESTADISTICA = "estadistica";
        public static final String NOMBRE = "nombre";
        public static final String FECHA = "fecha";
        public static final String DURACION = "duracion";
        public static final String JUGADOR = "jugador";
    }

    public static abstract class EstadisticaEntidad implements BaseColumns {
        public static final String TABLE_NAME = "estadisticas";

        public static final String ID = "id";
        public static final String GANADOS = "ganados";
        public static final String PERDIDOS = "perdidos";
        public static final String SET1 = "set1";
        public static final String SET2 = "set2";
        public static final String SET3 = "set3";
        public static final String SET4 = "set4";
        public static final String SET5 = "set5";
    }

    public static abstract class SetEntidad implements BaseColumns {
        public static final String TABLE_NAME = "sets";

        public static final String ID = "id";
        public static final String NUMERO = "numero";
        public static final String ESTADISTICAS = "estadisticas";
        public static final String GANADOS = "ganados";
        public static final String PERDIDOS = "perdidos";
        public static final String ADENTRO_GAN_D = "adentroGanD";
        public static final String ADENTRO_GAN_R = "adentroGanR";
        public static final String AFUERA_GAN_D = "afueraGanD";
        public static final String AFUERA_GAN_R = "afueraGanR";
        public static final String MALLA_GAN_D = "mallaGanD";
        public static final String MALLA_GAN_R = "mallaGanR";
        public static final String ADENTRO_PER_D = "adentroPerD";
        public static final String ADENTRO_PER_R = "adentroPerR";
        public static final String AFUERA_PER_D = "afueraPerD";
        public static final String AFUERA_PER_R = "afueraPerR";
        public static final String MALLA_PER_D = "mallaPerD";
        public static final String MALLA_PER_R = "mallaPerR";
    }
}
