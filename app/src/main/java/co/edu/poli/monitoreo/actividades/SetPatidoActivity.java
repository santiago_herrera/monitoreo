package co.edu.poli.monitoreo.actividades;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.androidplot.pie.PieChart;
import com.androidplot.pie.PieRenderer;
import com.androidplot.pie.Segment;
import com.androidplot.pie.SegmentFormatter;
import com.androidplot.util.PixelUtils;

import java.util.List;

import co.edu.poli.monitoreo.R;
import co.edu.poli.monitoreo.modelo.SetPartido;
import co.edu.poli.monitoreo.persistencia.AdminSQLiteOpenHelper;

public class SetPatidoActivity extends AppCompatActivity {

    private static String JUGADOR = "";
    private static String CORREO = "";
    private static String NUMERO = "";
    private String NOMBRE = "";
    private String ID = "";

    public static final int SELECTED_SEGMENT_OFFSET = 50;

    public PieChart pie;

    private Segment s1;
    private Segment s2;
    private Segment s3;
    private Segment s4;
    private Segment s5;
    private Segment s6;
    private Segment s7;
    private Segment s8;
    private Segment s9;
    private Segment s10;
    private Segment s11;
    private Segment s12;

    private Button mostrar;
    private Button cerrar;

    private SetPartido set;

    private TextView adentroGanD;
    private TextView adentroGanR;
    private TextView afueraGanD;
    private TextView afueraGanR;
    private TextView mallaGanD;
    private TextView mallaGanR;
    private TextView adentroPerD;
    private TextView adentroPerR;
    private TextView afueraPerD;
    private TextView afueraPerR;
    private TextView mallaPerD;
    private TextView mallaPerR;
    private TextView totalGan;
    private TextView totalPer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_patido);


        Intent intent = getIntent();
        NOMBRE = intent.getStringExtra("partido");
        NUMERO = intent.getIntExtra("numero", 1) + "";
        JUGADOR = intent.getStringExtra("jugador");
        ID = intent.getStringExtra("id");


        set = consulta();


        adentroGanD = findViewById(R.id.adentroGanD);
        adentroGanR = findViewById(R.id.adentroGanR);
        afueraGanD = findViewById(R.id.afueraGanD);
        afueraGanR = findViewById(R.id.afueraGanR);
        mallaGanD = findViewById(R.id.mallaGanD);
        mallaGanR = findViewById(R.id.mallaGanR);
        adentroPerD = findViewById(R.id.adentroPerD);
        adentroPerR = findViewById(R.id.adentroPerR);
        afueraPerD = findViewById(R.id.afueraPerD);
        afueraPerR = findViewById(R.id.afueraPerR);
        mallaPerD = findViewById(R.id.mallaPerD);
        mallaPerR = findViewById(R.id.mallaPerR);
        totalGan = findViewById(R.id.totalGan);
        totalPer = findViewById(R.id.totalPer);


        adentroGanD.setText("" + set.getAdentroGanD());
        adentroGanR.setText("" + set.getAdentroGanR());
        afueraGanD.setText("" + set.getAfueraGanD());
        afueraGanR.setText("" + set.getAfueraGanR());
        mallaGanD.setText("" + set.getMallaGanD());
        mallaGanR.setText("" + set.getMallaGanR());
        adentroPerD.setText("" + set.getAdentroPerD());
        adentroPerR.setText("" + set.getAdentroPerR());
        afueraPerD.setText("" + set.getAfueraPerD());
        afueraPerR.setText("" + set.getAfueraPerR());
        mallaPerD.setText("" + set.getMallaPerD());
        mallaPerR.setText("" + set.getMallaPerR());
        totalGan.setText("" + set.getGanados());
        totalPer.setText("" + set.getPerdidos());


        mostrar = findViewById(R.id.mostrar);
        mostrar.setOnClickListener(v -> {

            LayoutInflater inflater = (LayoutInflater) v.getContext().getSystemService(LAYOUT_INFLATER_SERVICE);

            // Inflate the custom layout/view
            View customView = inflater.inflate(R.layout.estadisticas, null);


            // Initialize a new instance of popup window
            PopupWindow mPopupWindow = new PopupWindow(
                    customView,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            );

            cerrar = customView.findViewById(R.id.cerrar);

            // Set a click listener for the popup window close button
            cerrar.setOnClickListener(view -> {
                // Dismiss the popup window
                mPopupWindow.dismiss();
            });
            mPopupWindow.showAtLocation(mostrar, Gravity.CENTER, 0, 0);


            pie = customView.findViewById(R.id.mySimplePieChart);
            pie.setTitle(NOMBRE);
            sets();


        });


    }

    public void sets() {


        // initialize our XYPlot reference:


        // enable the legend:
        pie.getLegend().setVisible(false);


        final float padding = PixelUtils.dpToPix(30);
        pie.getPie().setPadding(padding, padding, padding, padding);

        // detect segment clicks:
        pie.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                PointF click = new PointF(motionEvent.getX(), motionEvent.getY());
                if (pie.getPie().containsPoint(click)) {
                    Segment segment = pie.getRenderer(PieRenderer.class).getContainingSegment(click);

                    if (segment != null) {
                        final boolean isSelected = getFormatter(segment).getOffset() != 0;
                        deselectAll();
                        setSelected(segment, !isSelected);
                        pie.redraw();
                    }
                }
                return false;
            }

            private SegmentFormatter getFormatter(Segment segment) {
                return pie.getFormatter(segment, PieRenderer.class);
            }

            private void deselectAll() {
                List<Segment> segments = pie.getRegistry().getSeriesList();
                for (Segment segment : segments) {
                    setSelected(segment, false);
                }
            }

            private void setSelected(Segment segment, boolean isSelected) {
                SegmentFormatter f = getFormatter(segment);
                if (isSelected) {
                    f.setOffset(SELECTED_SEGMENT_OFFSET);
                } else {
                    f.setOffset(0);
                }
            }
        });


        s1 = new Segment("AdentroGanadosDrive", set.getAdentroGanD());
        s2 = new Segment("AdentroGanadosReves", set.getAdentroGanR());
        s3 = new Segment("AfueraGanadosDrive", set.getAfueraGanD());
        s4 = new Segment("AfueraGanadosReves", set.getAfueraGanR());
        s5 = new Segment("MallaGanadosDrive", set.getMallaGanD());
        s6 = new Segment("MallaGanadosReves", set.getMallaGanR());
        s7 = new Segment("AdentroPerdidosDrive", set.getAdentroPerD());
        s8 = new Segment("AdentroPerdidosReves", set.getAdentroPerR());
        s9 = new Segment("AfueraPerdidosDrive", set.getAfueraPerD());
        s10 = new Segment("AfueraPerdidosReves", set.getAfueraPerR());
        s11 = new Segment("MallaPerdidosDrive", set.getMallaPerD());
        s12 = new Segment("MallaPerdidosReves", set.getMallaPerR());

        EmbossMaskFilter emf = new EmbossMaskFilter(
                new float[]{1, 1, 1}, 0.4f, 10, 8.2f);

        SegmentFormatter sf1 = new SegmentFormatter(this, R.xml.pie_segment_formatter1);
        sf1.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf1.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf2 = new SegmentFormatter(this, R.xml.pie_segment_formatter1);
        sf2.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf2.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf3 = new SegmentFormatter(this, R.xml.pie_segment_formatter1);
        sf3.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf3.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf4 = new SegmentFormatter(this, R.xml.pie_segment_formatter1);
        sf4.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf4.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf5 = new SegmentFormatter(this, R.xml.pie_segment_formatter1);
        sf5.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf5.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf6 = new SegmentFormatter(this, R.xml.pie_segment_formatter1);
        sf6.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf6.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf7 = new SegmentFormatter(this, R.xml.pie_segment_formatter3);
        sf7.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf7.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf8 = new SegmentFormatter(this, R.xml.pie_segment_formatter3);
        sf8.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf8.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf9 = new SegmentFormatter(this, R.xml.pie_segment_formatter3);
        sf9.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf9.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf10 = new SegmentFormatter(this, R.xml.pie_segment_formatter3);
        sf10.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf10.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf11 = new SegmentFormatter(this, R.xml.pie_segment_formatter3);
        sf11.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf11.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf12 = new SegmentFormatter(this, R.xml.pie_segment_formatter3);
        sf12.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf12.getFillPaint().setMaskFilter(emf);

        pie.addSegment(s1, sf1);
        pie.addSegment(s2, sf2);
        pie.addSegment(s3, sf3);
        pie.addSegment(s4, sf4);
        pie.addSegment(s5, sf5);
        pie.addSegment(s6, sf6);
        pie.addSegment(s7, sf7);
        pie.addSegment(s8, sf8);
        pie.addSegment(s9, sf9);
        pie.addSegment(s10, sf10);
        pie.addSegment(s11, sf11);
        pie.addSegment(s12, sf12);

        pie.getBorderPaint().setColor(Color.TRANSPARENT);
        pie.getBackgroundPaint().setColor(Color.TRANSPARENT);
    }

    public SetPartido consulta() {


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);

        SQLiteDatabase bd = admin.getWritableDatabase();
        String[] args = new String[]{ID, NUMERO};
        Cursor fila = bd.rawQuery("SELECT * FROM sets WHERE estadisticas = ? AND numero = ?", args);
        SetPartido set = null;

        if (fila.moveToFirst()) {
            do {
                set = new SetPartido(
                        fila.getString(0),
                        fila.getString(1),
                        fila.getInt(2),
                        fila.getInt(3),
                        fila.getInt(4),
                        fila.getInt(5),
                        fila.getInt(6),
                        fila.getInt(7),
                        fila.getInt(8),
                        fila.getInt(9),
                        fila.getInt(10),
                        fila.getInt(11),
                        fila.getInt(12),
                        fila.getInt(13),
                        fila.getInt(14),
                        fila.getInt(15),
                        fila.getInt(16)
                );

            } while (fila.moveToNext());
        }

        fila.close();
        bd.close();

        return set;

    }
}
