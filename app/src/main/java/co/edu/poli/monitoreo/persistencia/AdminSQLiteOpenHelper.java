package co.edu.poli.monitoreo.persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AdminSQLiteOpenHelper extends SQLiteOpenHelper {
    public static final String NOMBRE_DB = "monitoreo.db";
    public static final int VERSION = 1;

    public AdminSQLiteOpenHelper(Context context) {
        super(context, NOMBRE_DB, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table partidos (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                " estadistica text," +
                " nombre text," +
                " fecha text," +
                " duracion text," +
                " jugador text" +
                ")");

        db.execSQL("create table jugadores (" +
                "id text primary key," +
                " nombres text," +
                " apellidos text," +
                " correo text," +
                " edad text," +
                " categoria text," +
                " entrenador text" +
                ")");

        db.execSQL("create table estadisticas (" +
                "id text primary key," +
                " ganados text," +
                " perdidos text," +
                " set1 text," +
                " set2 text," +
                " set3 text," +
                " set4 text," +
                " set5 text" +
                ")");

        db.execSQL("create table sets (" +
                "id text primary key," +
                " numero text," +
                " estadisticas text," +
                " ganados text," +
                " perdidos text," +
                " adentroGanD text," +
                " adentroGanR text," +
                " afueraGanD text," +
                " afueraGanR text," +
                " mallaGanD text," +
                " mallaGanR text," +
                " adentroPerD text," +
                " adentroPerR text," +
                " afueraPerD text," +
                " afueraPerR text," +
                " mallaPerD text," +
                " mallaPerR text" +
                ")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int version1, int version2) {

        db.execSQL("drop table if exists jugadores");
        db.execSQL("drop table if exists partidos");

        db.execSQL("create table jugadores(" +
                "id text primary key," +
                " nombres text," +
                " apellidos text," +
                " correo text," +
                " edad text," +
                " categoria text," +
                " entrenador text" +
                ")");

        db.execSQL("create table partidos(" +
                "id INTEGER primary key," +
                " estadistica text," +
                " nombre text," +
                " fecha text," +
                " duracion text," +
                " jugador text" +
                ")");
    }

}