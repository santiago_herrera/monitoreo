package co.edu.poli.monitoreo.actividades;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import co.edu.poli.monitoreo.R;
import co.edu.poli.monitoreo.modelo.Jugador;
import co.edu.poli.monitoreo.persistencia.AdminSQLiteOpenHelper;
import co.edu.poli.monitoreo.persistencia.Contratos;

public class AgregarJugador extends AppCompatActivity {

    private static String ENTRENADOR = "";
    private EditText edtNombres;
    private EditText edtApellidos;
    private EditText edtCorreo;
    private EditText edtEdad;
    private EditText edtCategoria;
    private Button btnGuardar;
    private Button btnCancelar;
    public ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_jugador);

        Intent intent = getIntent();
        ENTRENADOR = intent.getStringExtra("entrenador");

        edtNombres = findViewById(R.id.edtNombres);
        edtApellidos = findViewById(R.id.edtApellidos);
        edtCorreo = findViewById(R.id.edtCorreo);
        edtEdad = findViewById(R.id.edtEdad);
        edtCategoria = findViewById(R.id.edtCategoria);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnCancelar = findViewById(R.id.btnCancelar);

        btnGuardar.setOnClickListener(v -> agregarJugador());

        btnCancelar.setOnClickListener(v -> finish());


        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);


    }

    private void agregarJugador() {
        if (!validateForm()) {
            return;
        }
        showProgressDialog();

        String nombres = edtNombres.getText().toString();
        String apellidos = edtApellidos.getText().toString();
        String correo = edtCorreo.getText().toString();
        String edad = edtEdad.getText().toString();
        String categoria = edtCategoria.getText().toString();

        Jugador jugador = new Jugador(nombres, apellidos, correo, edad, categoria, ENTRENADOR);

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);

        SQLiteDatabase bd = admin.getWritableDatabase();

        // los inserto en la base de datos
        bd.insert(Contratos.JugadorEntidad.TABLE_NAME, null, jugador.toContentValues());

        bd.close();

        Toast.makeText(this, "Datos del jugador guardados", Toast.LENGTH_SHORT).show();

        edtNombres.setText("");
        edtApellidos.setText("");
        edtCorreo.setText("");
        edtEdad.setText("");
        edtCategoria.setText("");

        hideProgressDialog();

    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Cargando");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private boolean validateForm() {
        boolean valid = true;

        String categoria = edtCategoria.getText().toString();
        if (TextUtils.isEmpty(categoria)) {
            edtCategoria.setError("Requerido.");
            valid = false;
        } else {
            edtCategoria.setError(null);
        }

        String nombres = edtNombres.getText().toString();
        if (TextUtils.isEmpty(nombres)) {
            edtNombres.setError("Requerido.");
            valid = false;
        } else {
            edtNombres.setError(null);
        }

        String apellidos = edtApellidos.getText().toString();
        if (TextUtils.isEmpty(apellidos)) {
            edtApellidos.setError("Requerido.");
            valid = false;
        } else {
            edtApellidos.setError(null);
        }

        String correo = edtCorreo.getText().toString();
        if (TextUtils.isEmpty(correo)) {
            edtCorreo.setError("Requerido.");
            valid = false;
        } else {
            edtCorreo.setError(null);
        }

        String edad = edtEdad.getText().toString();
        if (TextUtils.isEmpty(edad)) {
            edtEdad.setError("Requerido.");
            valid = false;
        } else {
            edtEdad.setError(null);
        }

        return valid;
    }

}
