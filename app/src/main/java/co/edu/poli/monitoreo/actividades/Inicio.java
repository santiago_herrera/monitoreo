package co.edu.poli.monitoreo.actividades;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import co.edu.poli.monitoreo.R;

public class Inicio extends AppCompatActivity {

    private final String TAG = this.getClass().getName();
    private FirebaseAuth mAuth;

    private EditText edtCorreo;
    private EditText edtContrasena;
    public ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Views
        edtCorreo = findViewById(R.id.edtCorreo);
        edtContrasena = findViewById(R.id.edtContrasena);

        // Buttons
        Button btnIngresar = findViewById(R.id.btnIngresar);
        Button btnRegistrar = findViewById(R.id.btnRegistrar);
        Button btnRecordar = findViewById(R.id.btnRecordar);

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]


        btnIngresar.setOnClickListener(v -> signIn(edtCorreo.getText().toString(), edtContrasena.getText().toString()));

        btnRegistrar.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), Registrar.class);
            startActivity(intent);
        });

        btnRecordar.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), Recordar.class);
            startActivity(intent);
        });

    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Cargando");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(Inicio.this, "Error en los datos.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }


                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with_email]
    }

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }


    private boolean validateForm() {
        boolean valid = true;

        String email = edtCorreo.getText().toString();
        if (TextUtils.isEmpty(email)) {
            edtCorreo.setError("Requerido.");
            valid = false;
        } else {
            edtCorreo.setError(null);
        }

        String password = edtContrasena.getText().toString();
        if (TextUtils.isEmpty(password)) {
            edtContrasena.setError("Requerido.");
            valid = false;
        } else {
            edtContrasena.setError(null);
        }

        return valid;
    }

    private void updateUI(FirebaseUser user) {
        hideProgressDialog();
        if (user != null) {
            Intent intent = new Intent(getApplicationContext(), Ingresar.class);
            String message = user.getEmail();
            intent.putExtra("correo", message);
            startActivity(intent);
        }
    }
}
