package co.edu.poli.monitoreo.actividades;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;

import co.edu.poli.monitoreo.R;

public class Recordar extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private final String TAG = this.getClass().getName();

    private EditText edtCorreo;
    private Button btnRecordad;
    private Button btnCancelar;
    public ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recordar);

        edtCorreo = findViewById(R.id.edtCorreo);
        btnRecordad = findViewById(R.id.btnRecordar);
        btnCancelar = findViewById(R.id.btnCancelar);

        mAuth = FirebaseAuth.getInstance();

        btnRecordad.setOnClickListener(v -> recordar(edtCorreo.getText().toString()));

        btnCancelar.setOnClickListener(v -> finish());
    }

    private void recordar(String email) {
        Log.d(TAG, "recordando :" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "Email enviado.");
                    }
                    hideProgressDialog();
                });
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = edtCorreo.getText().toString();
        if (TextUtils.isEmpty(email)) {
            edtCorreo.setError("Requerido.");
            valid = false;
        } else {
            edtCorreo.setError(null);
        }
        return valid;
    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Cargando");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
