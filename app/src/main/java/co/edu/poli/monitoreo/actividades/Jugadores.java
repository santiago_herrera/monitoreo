package co.edu.poli.monitoreo.actividades;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import co.edu.poli.monitoreo.R;
import co.edu.poli.monitoreo.persistencia.AdminSQLiteOpenHelper;

public class Jugadores extends AppCompatActivity {

    private static final int NUMERO = 1;
    private ArrayAdapter<String> adaptador;
    ListView lista;
    Button btnCancelar;

    private static String ENTRENADOR = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jugadores);

        Intent intent = getIntent();
        ENTRENADOR = intent.getStringExtra("entrenador");

        lista = findViewById(R.id.listaJugadores);
        btnCancelar = findViewById(R.id.btnCancelar);
        Button btnAgregar = findViewById(R.id.btnAgregar);
        adaptador = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);

        btnAgregar.setOnClickListener(v -> {
            Intent intent1 = new Intent(getApplicationContext(), AgregarJugador.class);
            intent1.putExtra("entrenador", ENTRENADOR);
            startActivityForResult(intent1, NUMERO);
        });

        consulta();


//        lista.setOnItemClickListener((parent, view, position, id) ->
//                Toast.makeText(getApplicationContext(), "posicion " + position, Toast.LENGTH_LONG).show());

        btnCancelar.setOnClickListener(v -> finish());

    }

    public void consulta() {
        adaptador.clear();

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);

        SQLiteDatabase bd = admin.getWritableDatabase();
        String[] args = new String[]{ENTRENADOR};
        Cursor fila = bd.rawQuery("SELECT nombres,apellidos FROM jugadores WHERE entrenador =?", args);


        if (fila.moveToFirst()) {
            do {
                agregarLista(fila.getString(0) + " " + fila.getString(1));
            } while (fila.moveToNext());
        }

        bd.close();

    }

    private void agregarLista(String nombreCompleato) {
        adaptador.add(nombreCompleato);
        lista.setAdapter(adaptador);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == NUMERO) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                consulta();
            }
        }
    }
}
