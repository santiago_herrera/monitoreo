package co.edu.poli.monitoreo.modelo;

import java.util.List;
import java.util.UUID;

public class Entrenador {
    private String uuid;
    private String nombres;
    private String apellidos;
    private String correo;
    private List<Jugador> jugadores;

    public Entrenador(String nombres, String apellidos, String correo, List<Jugador> jugadores) {
        this.uuid =  UUID.randomUUID().toString();
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.jugadores = jugadores;
    }

    public String getUuid() {
        return uuid;
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public List<Jugador> getJugadores() {
        return jugadores;
    }

}
