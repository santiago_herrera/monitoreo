package co.edu.poli.monitoreo.actividades;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import co.edu.poli.monitoreo.R;
import co.edu.poli.monitoreo.modelo.Partido;
import co.edu.poli.monitoreo.persistencia.AdminSQLiteOpenHelper;

public class Partidos extends AppCompatActivity {

    private static final int NUMERO = 1;
    private ArrayAdapter<String> adaptador;
    ListView lista;
    List<Partido> partidos;
    Button btnCancelar;

    private static String JUGADOR = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partidos);

        Intent intent = getIntent();
        JUGADOR = intent.getStringExtra("jugador");

        lista = findViewById(R.id.listaPartidos);
        adaptador = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        consulta();
        lista.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent1 = new Intent(getApplicationContext(), EstadisticaActivity.class);
            intent1.putExtra("partido", partidos.get(position).getNombre());
            intent1.putExtra("id", partidos.get(position).getEstadistica());
            intent1.putExtra("jugador", JUGADOR);
            intent1.putExtra("atras", 1);
            startActivity(intent1);

        });
        btnCancelar = findViewById(R.id.btnCancelar);

        btnCancelar.setOnClickListener(v -> finish());


        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
    }

    public void consulta() {
        adaptador.clear();
        partidos = new ArrayList<>();


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);

        SQLiteDatabase bd = admin.getWritableDatabase();
        String[] args = new String[]{JUGADOR};
        Cursor fila = bd.rawQuery("SELECT * FROM partidos WHERE jugador =?", args);


        if (fila.moveToFirst()) {
            do {
                Partido partido = new Partido(
                        fila.getInt(0),
                        fila.getString(1),
                        fila.getString(2),
                        fila.getString(3),
                        fila.getString(4),
                        fila.getString(5));

                partidos.add(partido);
                agregarLista(partido.getNombre() + " " + partido.getFecha());
            } while (fila.moveToNext());
        }

        fila.close();
        bd.close();

    }

    private void agregarLista(String nombreCompleato) {
        adaptador.add(nombreCompleato);
        lista.setAdapter(adaptador);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == NUMERO) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                consulta();
            }
        }
    }
}
