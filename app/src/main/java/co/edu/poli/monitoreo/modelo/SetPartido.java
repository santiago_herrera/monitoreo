package co.edu.poli.monitoreo.modelo;

import android.content.ContentValues;

import java.util.UUID;

import co.edu.poli.monitoreo.persistencia.Contratos;

public class SetPartido {

    private String id;
    private String estadisticas;
    private Integer numero;
    private Integer ganados;
    private Integer perdidos;
    private Integer adentroGanD;
    private Integer adentroGanR;
    private Integer afueraGanD;
    private Integer afueraGanR;
    private Integer mallaGanD;
    private Integer mallaGanR;
    private Integer adentroPerD;
    private Integer adentroPerR;
    private Integer afueraPerD;
    private Integer afueraPerR;
    private Integer mallaPerD;
    private Integer mallaPerR;

    public SetPartido(String id, String estadisticas, Integer numero, Integer ganados, Integer perdidos, Integer adentroGanD, Integer adentroGanR, Integer afueraGanD, Integer afueraGanR, Integer mallaGanD, Integer mallaGanR, Integer adentroPerD, Integer adentroPerR, Integer afueraPerD, Integer afueraPerR, Integer mallaPerD, Integer mallaPerR) {
        this.id = id;
        this.estadisticas = estadisticas;
        this.numero = numero;
        this.ganados = ganados;
        this.perdidos = perdidos;
        this.adentroGanD = adentroGanD;
        this.adentroGanR = adentroGanR;
        this.afueraGanD = afueraGanD;
        this.afueraGanR = afueraGanR;
        this.mallaGanD = mallaGanD;
        this.mallaGanR = mallaGanR;
        this.adentroPerD = adentroPerD;
        this.adentroPerR = adentroPerR;
        this.afueraPerD = afueraPerD;
        this.afueraPerR = afueraPerR;
        this.mallaPerD = mallaPerD;
        this.mallaPerR = mallaPerR;
    }

    public SetPartido(String estadisticas, Integer numero, Integer ganados, Integer perdidos, Integer adentroGanD, Integer adentroGanR, Integer afueraGanD, Integer afueraGanR, Integer mallaGanD, Integer mallaGanR, Integer adentroPerD, Integer adentroPerR, Integer afueraPerD, Integer afueraPerR, Integer mallaPerD, Integer mallaPerR) {
        this.numero = numero;
        this.id = UUID.randomUUID().toString();
        this.estadisticas = estadisticas;
        this.ganados = ganados;
        this.perdidos = perdidos;
        this.adentroGanD = adentroGanD;
        this.adentroGanR = adentroGanR;
        this.afueraGanD = afueraGanD;
        this.afueraGanR = afueraGanR;
        this.mallaGanD = mallaGanD;
        this.mallaGanR = mallaGanR;
        this.adentroPerD = adentroPerD;
        this.adentroPerR = adentroPerR;
        this.afueraPerD = afueraPerD;
        this.afueraPerR = afueraPerR;
        this.mallaPerD = mallaPerD;
        this.mallaPerR = mallaPerR;
    }


    public String getId() {
        return id;
    }

    public String getEstadisticas() {
        return estadisticas;
    }

    public Integer getGanados() {
        return ganados;
    }

    public Integer getPerdidos() {
        return perdidos;
    }

    public Integer getAdentroGanD() {
        return adentroGanD;
    }

    public Integer getAdentroGanR() {
        return adentroGanR;
    }

    public Integer getAfueraGanD() {
        return afueraGanD;
    }

    public Integer getAfueraGanR() {
        return afueraGanR;
    }

    public Integer getMallaGanD() {
        return mallaGanD;
    }

    public Integer getMallaGanR() {
        return mallaGanR;
    }

    public Integer getAdentroPerD() {
        return adentroPerD;
    }

    public Integer getAdentroPerR() {
        return adentroPerR;
    }

    public Integer getAfueraPerD() {
        return afueraPerD;
    }

    public Integer getAfueraPerR() {
        return afueraPerR;
    }

    public Integer getMallaPerD() {
        return mallaPerD;
    }

    public Integer getMallaPerR() {
        return mallaPerR;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(Contratos.SetEntidad.ID, id);
        values.put(Contratos.SetEntidad.NUMERO, numero);
        values.put(Contratos.SetEntidad.ESTADISTICAS, estadisticas);
        values.put(Contratos.SetEntidad.GANADOS, ganados);
        values.put(Contratos.SetEntidad.PERDIDOS, perdidos);
        values.put(Contratos.SetEntidad.ADENTRO_GAN_D, adentroGanD);
        values.put(Contratos.SetEntidad.ADENTRO_GAN_R, adentroGanR);
        values.put(Contratos.SetEntidad.AFUERA_GAN_D, afueraGanD);
        values.put(Contratos.SetEntidad.AFUERA_GAN_R, afueraGanR);
        values.put(Contratos.SetEntidad.MALLA_GAN_D, mallaGanD);
        values.put(Contratos.SetEntidad.MALLA_GAN_R, mallaGanR);
        values.put(Contratos.SetEntidad.ADENTRO_PER_D, adentroPerD);
        values.put(Contratos.SetEntidad.ADENTRO_PER_R, adentroPerR);
        values.put(Contratos.SetEntidad.AFUERA_PER_D, afueraPerD);
        values.put(Contratos.SetEntidad.AFUERA_PER_R, afueraPerR);
        values.put(Contratos.SetEntidad.MALLA_PER_D, mallaPerD);
        values.put(Contratos.SetEntidad.MALLA_PER_R, mallaPerR);
        return values;
    }

    public Integer getNumero() {
        return numero;
    }
}
