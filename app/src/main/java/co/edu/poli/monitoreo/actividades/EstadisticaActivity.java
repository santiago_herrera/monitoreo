package co.edu.poli.monitoreo.actividades;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.androidplot.pie.PieChart;
import com.androidplot.pie.PieRenderer;
import com.androidplot.pie.Segment;
import com.androidplot.pie.SegmentFormatter;
import com.androidplot.util.PixelUtils;

import java.util.List;

import co.edu.poli.monitoreo.R;
import co.edu.poli.monitoreo.modelo.Estadistica;
import co.edu.poli.monitoreo.modelo.SetPartido;
import co.edu.poli.monitoreo.persistencia.AdminSQLiteOpenHelper;

public class EstadisticaActivity extends AppCompatActivity {
    private static String JUGADOR = "";
    private static String CORREO = "";
    private static Integer ATRAS = 0;
    private String NOMBRE = "";
    private String ID = "";

    private Button set1, set2, set3, set4, set5;
    public static final int SELECTED_SEGMENT_OFFSET = 50;


    public PieChart pie;

    private Segment s1;
    private Segment s2;
    private Segment s3;
    private Segment s4;
    private Segment s5;
    private Segment s6;
    private Segment s7;
    private Segment s8;
    private Segment s9;
    private Segment s10;
    private Segment s11;
    private Segment s12;

    private Button mostrar;
    private Button cerrar;

    private TextView adentroGanD;
    private TextView adentroGanR;
    private TextView afueraGanD;
    private TextView afueraGanR;
    private TextView mallaGanD;
    private TextView mallaGanR;
    private TextView adentroPerD;
    private TextView adentroPerR;
    private TextView afueraPerD;
    private TextView afueraPerR;
    private TextView mallaPerD;
    private TextView mallaPerR;
    private TextView totalGanadosTotales;
    private TextView totalPerdidosTotales;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estadistica);

        Intent intent = getIntent();
        NOMBRE = intent.getStringExtra("partido");
        JUGADOR = intent.getStringExtra("jugador");
        ID = intent.getStringExtra("id");
        ATRAS = intent.getIntExtra("atras", 0);


        set1 = findViewById(R.id.set1);
        set2 = findViewById(R.id.set2);
        set3 = findViewById(R.id.set3);
        set4 = findViewById(R.id.set4);
        set5 = findViewById(R.id.set5);


        adentroGanD = findViewById(R.id.adentroGanD);
        adentroGanR = findViewById(R.id.adentroGanR);
        afueraGanD = findViewById(R.id.afueraGanD);
        afueraGanR = findViewById(R.id.afueraGanR);
        mallaGanD = findViewById(R.id.mallaGanD);
        mallaGanR = findViewById(R.id.mallaGanR);
        adentroPerD = findViewById(R.id.adentroPerD);
        adentroPerR = findViewById(R.id.adentroPerR);
        afueraPerD = findViewById(R.id.afueraPerD);
        afueraPerR = findViewById(R.id.afueraPerR);
        mallaPerD = findViewById(R.id.mallaPerD);
        mallaPerR = findViewById(R.id.mallaPerR);
        totalGanadosTotales = findViewById(R.id.totalGanados);
        totalPerdidosTotales = findViewById(R.id.totalPerdidos);

        Estadistica estadistica = consulta();
        variables(estadistica);

        totalGanadosTotales.setText("" + estadistica.getGanados());
        totalPerdidosTotales.setText("" + estadistica.getPerdidos());

        mostrar = findViewById(R.id.mostrar);
        mostrar.setOnClickListener(v -> {

            LayoutInflater inflater = (LayoutInflater) v.getContext().getSystemService(LAYOUT_INFLATER_SERVICE);

            // Inflate the custom layout/view
            View customView = inflater.inflate(R.layout.estadisticas, null);


            // Initialize a new instance of popup window
            PopupWindow mPopupWindow = new PopupWindow(
                    customView,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            );

            cerrar = customView.findViewById(R.id.cerrar);

            // Set a click listener for the popup window close button
            cerrar.setOnClickListener(view -> {
                // Dismiss the popup window
                mPopupWindow.dismiss();
            });
            mPopupWindow.showAtLocation(mostrar, Gravity.CENTER, 0, 0);


            pie = customView.findViewById(R.id.mySimplePieChart);
            pie.setTitle(NOMBRE);
            sets(estadistica);


        });


        set1.setOnClickListener(v -> {
            Intent intent1 = new Intent(getApplicationContext(), SetPatidoActivity.class);
            intent1.putExtra("partido", NOMBRE);
            intent1.putExtra("jugador", JUGADOR);
            intent1.putExtra("id", ID);
            intent1.putExtra("numero", 1);
            startActivity(intent1);
        });

        set2.setOnClickListener(v -> {
            Intent intent1 = new Intent(getApplicationContext(), SetPatidoActivity.class);
            intent1.putExtra("partido", NOMBRE);
            intent1.putExtra("jugador", JUGADOR);
            intent1.putExtra("id", ID);
            intent1.putExtra("numero", 2);
            startActivity(intent1);
        });

        set3.setOnClickListener(v -> {
            Intent intent1 = new Intent(getApplicationContext(), SetPatidoActivity.class);
            intent1.putExtra("partido", NOMBRE);
            intent1.putExtra("jugador", JUGADOR);
            intent1.putExtra("id", ID);
            intent1.putExtra("numero", 3);
            startActivity(intent1);
        });

        set4.setOnClickListener(v -> {
            Intent intent1 = new Intent(getApplicationContext(), SetPatidoActivity.class);
            intent1.putExtra("partido", NOMBRE);
            intent1.putExtra("jugador", JUGADOR);
            intent1.putExtra("id", ID);
            intent1.putExtra("numero", 4);
            startActivity(intent1);
        });

        set5.setOnClickListener(v -> {
            Intent intent1 = new Intent(getApplicationContext(), SetPatidoActivity.class);
            intent1.putExtra("partido", NOMBRE);
            intent1.putExtra("jugador", JUGADOR);
            intent1.putExtra("id", ID);
            intent1.putExtra("numero", 5);
            startActivity(intent1);
        });


    }

    public Estadistica consulta() {


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);

        SQLiteDatabase bd = admin.getWritableDatabase();
        String[] args = new String[]{ID};
        Cursor fila = bd.rawQuery("SELECT * FROM estadisticas WHERE id = ?", args);
        Estadistica estadistica = null;

        if (fila.moveToFirst()) {
            do {
                estadistica = new Estadistica(
                        fila.getString(0),
                        fila.getInt(1),
                        fila.getInt(2),
                        consulta("1"),
                        consulta("2"),
                        consulta("3"),
                        consulta("4"),
                        consulta("5")
                );

            } while (fila.moveToNext());
        }

        fila.close();
        bd.close();

        return estadistica;

    }

    public SetPartido consulta(String numero) {


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);

        SQLiteDatabase bd = admin.getWritableDatabase();
        String[] args = new String[]{ID, numero};
        Cursor fila = bd.rawQuery("SELECT * FROM sets WHERE estadisticas = ? AND numero = ?", args);
        SetPartido set = null;

        if (fila.moveToFirst()) {
            do {
                set = new SetPartido(
                        fila.getString(0),
                        fila.getString(1),
                        fila.getInt(2),
                        fila.getInt(3),
                        fila.getInt(4),
                        fila.getInt(5),
                        fila.getInt(6),
                        fila.getInt(7),
                        fila.getInt(8),
                        fila.getInt(9),
                        fila.getInt(10),
                        fila.getInt(11),
                        fila.getInt(12),
                        fila.getInt(13),
                        fila.getInt(14),
                        fila.getInt(15),
                        fila.getInt(16)
                );

            } while (fila.moveToNext());
        }

        fila.close();
        bd.close();

        return set;

    }


    @Override
    public void onBackPressed() {
        if (ATRAS == 1) {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.estadistica, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.terminar:
                consultaEntrenador();
                Intent intent1 = new Intent(getApplicationContext(), Ingresar.class);
                intent1.putExtra("correo", CORREO);
                startActivity(intent1);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void consultaEntrenador() {


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);

        SQLiteDatabase bd = admin.getWritableDatabase();
        String[] args = new String[]{JUGADOR};
        Cursor fila = bd.rawQuery("SELECT entrenador FROM jugadores WHERE id =?", args);


        if (fila.moveToFirst()) {
            do {
                CORREO = fila.getString(0);


            } while (fila.moveToNext());
        }

        fila.close();
        bd.close();

    }

    public void sets(Estadistica estadistica) {


        // initialize our XYPlot reference:


        // enable the legend:
        pie.getLegend().setVisible(false);


        final float padding = PixelUtils.dpToPix(30);
        pie.getPie().setPadding(padding, padding, padding, padding);

        // detect segment clicks:
        pie.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                PointF click = new PointF(motionEvent.getX(), motionEvent.getY());
                if (pie.getPie().containsPoint(click)) {
                    Segment segment = pie.getRenderer(PieRenderer.class).getContainingSegment(click);

                    if (segment != null) {
                        final boolean isSelected = getFormatter(segment).getOffset() != 0;
                        deselectAll();
                        setSelected(segment, !isSelected);
                        pie.redraw();
                    }
                }
                return false;
            }

            private SegmentFormatter getFormatter(Segment segment) {
                return pie.getFormatter(segment, PieRenderer.class);
            }

            private void deselectAll() {
                List<Segment> segments = pie.getRegistry().getSeriesList();
                for (Segment segment : segments) {
                    setSelected(segment, false);
                }
            }

            private void setSelected(Segment segment, boolean isSelected) {
                SegmentFormatter f = getFormatter(segment);
                if (isSelected) {
                    f.setOffset(SELECTED_SEGMENT_OFFSET);
                } else {
                    f.setOffset(0);
                }
            }
        });

        variables(estadistica);

        EmbossMaskFilter emf = new EmbossMaskFilter(
                new float[]{1, 1, 1}, 0.4f, 10, 8.2f);

        SegmentFormatter sf1 = new SegmentFormatter(this, R.xml.pie_segment_formatter1);
        sf1.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf1.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf2 = new SegmentFormatter(this, R.xml.pie_segment_formatter1);
        sf2.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf2.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf3 = new SegmentFormatter(this, R.xml.pie_segment_formatter1);
        sf3.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf3.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf4 = new SegmentFormatter(this, R.xml.pie_segment_formatter1);
        sf4.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf4.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf5 = new SegmentFormatter(this, R.xml.pie_segment_formatter1);
        sf5.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf5.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf6 = new SegmentFormatter(this, R.xml.pie_segment_formatter1);
        sf6.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf6.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf7 = new SegmentFormatter(this, R.xml.pie_segment_formatter3);
        sf7.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf7.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf8 = new SegmentFormatter(this, R.xml.pie_segment_formatter3);
        sf8.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf8.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf9 = new SegmentFormatter(this, R.xml.pie_segment_formatter3);
        sf9.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf9.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf10 = new SegmentFormatter(this, R.xml.pie_segment_formatter3);
        sf10.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf10.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf11 = new SegmentFormatter(this, R.xml.pie_segment_formatter3);
        sf11.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf11.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf12 = new SegmentFormatter(this, R.xml.pie_segment_formatter3);
        sf12.getLabelPaint().setShadowLayer(3, 0, 0, Color.BLACK);
        sf12.getFillPaint().setMaskFilter(emf);

        pie.addSegment(s1, sf1);
        pie.addSegment(s2, sf2);
        pie.addSegment(s3, sf3);
        pie.addSegment(s4, sf4);
        pie.addSegment(s5, sf5);
        pie.addSegment(s6, sf6);
        pie.addSegment(s7, sf7);
        pie.addSegment(s8, sf8);
        pie.addSegment(s9, sf9);
        pie.addSegment(s10, sf10);
        pie.addSegment(s11, sf11);
        pie.addSegment(s12, sf12);

        pie.getBorderPaint().setColor(Color.TRANSPARENT);
        pie.getBackgroundPaint().setColor(Color.TRANSPARENT);
    }

    private void variables(Estadistica estadistica) {
        Integer AdentroGanadosDrive = estadistica.getSetPartido1().getAdentroGanD()
                + estadistica.getSetPartido2().getAdentroGanD()
                + estadistica.getSetPartido3().getAdentroGanD()
                + estadistica.getSetPartido4().getAdentroGanD()
                + estadistica.getSetPartido5().getAdentroGanD();

        Integer AdentroGanadosReves = estadistica.getSetPartido1().getAdentroGanR()
                + estadistica.getSetPartido2().getAdentroGanR()
                + estadistica.getSetPartido3().getAdentroGanR()
                + estadistica.getSetPartido4().getAdentroGanR()
                + estadistica.getSetPartido5().getAdentroGanR();

        Integer AfueraGanadosDrive = estadistica.getSetPartido1().getAfueraGanD()
                + estadistica.getSetPartido2().getAfueraGanD()
                + estadistica.getSetPartido3().getAfueraGanD()
                + estadistica.getSetPartido4().getAfueraGanD()
                + estadistica.getSetPartido5().getAfueraGanD();

        Integer AfueraGanadosReves = estadistica.getSetPartido1().getAfueraGanR()
                + estadistica.getSetPartido2().getAfueraGanR()
                + estadistica.getSetPartido3().getAfueraGanR()
                + estadistica.getSetPartido4().getAfueraGanR()
                + estadistica.getSetPartido5().getAfueraGanR();

        Integer MallaGanadosDrive = estadistica.getSetPartido1().getMallaGanD()
                + estadistica.getSetPartido2().getMallaGanD()
                + estadistica.getSetPartido3().getMallaGanD()
                + estadistica.getSetPartido4().getMallaGanD()
                + estadistica.getSetPartido5().getMallaGanD();

        Integer MallaGanadosReves = estadistica.getSetPartido1().getMallaGanR()
                + estadistica.getSetPartido2().getMallaGanR()
                + estadistica.getSetPartido3().getMallaGanR()
                + estadistica.getSetPartido4().getMallaGanR()
                + estadistica.getSetPartido5().getMallaGanR();

        Integer AdentroPerdidosDrive = estadistica.getSetPartido1().getAdentroPerD()
                + estadistica.getSetPartido2().getAdentroPerD()
                + estadistica.getSetPartido3().getAdentroPerD()
                + estadistica.getSetPartido4().getAdentroPerD()
                + estadistica.getSetPartido5().getAdentroPerD();

        Integer AdentroPerdidosReves = estadistica.getSetPartido1().getAdentroPerR()
                + estadistica.getSetPartido2().getAdentroPerR()
                + estadistica.getSetPartido3().getAdentroPerR()
                + estadistica.getSetPartido4().getAdentroPerR()
                + estadistica.getSetPartido5().getAdentroPerR();

        Integer AfueraPerdidosDrive = estadistica.getSetPartido1().getAfueraPerD()
                + estadistica.getSetPartido2().getAfueraPerD()
                + estadistica.getSetPartido3().getAfueraPerD()
                + estadistica.getSetPartido4().getAfueraPerD()
                + estadistica.getSetPartido5().getAfueraPerD();

        Integer AfueraPerdidosReves = estadistica.getSetPartido1().getAfueraPerR()
                + estadistica.getSetPartido2().getAfueraPerR()
                + estadistica.getSetPartido3().getAfueraPerR()
                + estadistica.getSetPartido4().getAfueraPerR()
                + estadistica.getSetPartido5().getAfueraPerR();

        Integer MallaPerdidosDrive = estadistica.getSetPartido1().getMallaPerD()
                + estadistica.getSetPartido2().getMallaPerD()
                + estadistica.getSetPartido3().getMallaPerD()
                + estadistica.getSetPartido4().getMallaPerD()
                + estadistica.getSetPartido5().getMallaPerD();

        Integer MallaPerdidosReves = estadistica.getSetPartido1().getMallaPerR()
                + estadistica.getSetPartido2().getMallaPerR()
                + estadistica.getSetPartido3().getMallaPerR()
                + estadistica.getSetPartido4().getMallaPerR()
                + estadistica.getSetPartido5().getMallaPerR();

        adentroGanD.setText("" + AdentroGanadosDrive);
        adentroGanR.setText("" + AdentroGanadosReves);
        afueraGanD.setText("" + AfueraGanadosDrive);
        afueraGanR.setText("" + AfueraGanadosReves);
        mallaGanD.setText("" + MallaGanadosDrive);
        mallaGanR.setText("" + MallaGanadosReves);
        adentroPerD.setText("" + AdentroPerdidosDrive);
        adentroPerR.setText("" + AdentroPerdidosReves);
        afueraPerD.setText("" + AfueraPerdidosDrive);
        afueraPerR.setText("" + AfueraPerdidosReves);
        mallaPerD.setText("" + MallaPerdidosDrive);
        mallaPerR.setText("" + MallaPerdidosReves);


        s1 = new Segment("AdentroGanadosDrive", AdentroGanadosDrive);
        s2 = new Segment("AdentroGanadosReves", AdentroGanadosReves);
        s3 = new Segment("AfueraGanadosDrive", AfueraGanadosDrive);
        s4 = new Segment("AfueraGanadosReves", AfueraGanadosReves);
        s5 = new Segment("MallaGanadosDrive", MallaGanadosDrive);
        s6 = new Segment("MallaGanadosReves", MallaGanadosReves);
        s7 = new Segment("AdentroPerdidosDrive", AdentroPerdidosDrive);
        s8 = new Segment("AdentroPerdidosReves", AdentroPerdidosReves);
        s9 = new Segment("AfueraPerdidosDrive", AfueraPerdidosDrive);
        s10 = new Segment("AfueraPerdidosReves", AfueraPerdidosReves);
        s11 = new Segment("MallaPerdidosDrive", MallaPerdidosDrive);
        s12 = new Segment("MallaPerdidosReves", MallaPerdidosReves);
    }


}
