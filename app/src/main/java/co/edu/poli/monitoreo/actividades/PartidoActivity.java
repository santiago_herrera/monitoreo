package co.edu.poli.monitoreo.actividades;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import co.edu.poli.monitoreo.R;
import co.edu.poli.monitoreo.modelo.Estadistica;
import co.edu.poli.monitoreo.modelo.Partido;
import co.edu.poli.monitoreo.modelo.SetPartido;
import co.edu.poli.monitoreo.persistencia.AdminSQLiteOpenHelper;
import co.edu.poli.monitoreo.persistencia.Contratos;

public class PartidoActivity extends AppCompatActivity {

    private static String NOMBRE_JUGADOR = "";
    private static String ENTRENADOR = "";
    private static String ID = "";
    private static String JUGADOR = "";
    private static String NOMBRE_PARTIDO = "";

    private static String ADENTRO_GANADO_DRIVE = "adentroGanadoDrive";
    private static String ADENTRO_GANADO_REVES = "adentroGanadoReves";
    private static String AFUERA_GANADO_DRIVE = "afueraGanadoDrive";
    private static String AFUERA_GANADO_REVES = "afueraGanadoReves";
    private static String MALLA_GANADO_DRIVE = "mallaGanadoDrive";
    private static String MALLA_GANADO_REVES = "mallaGanadoReves";

    private static String ADENTRO_PERDIDO_DRIVE = "adentroPerdidoDrive";
    private static String ADENTRO_PERDIDO_REVES = "adentroPerdidoReves";
    private static String AFUERA_PERDIDO_DRIVE = "afueraPerdidoDrive";
    private static String AFUERA_PERDIDO_REVES = "afueraPerdidoReves";
    private static String MALLA_PERDIDO_DRIVE = "mallaPerdidoDrive";
    private static String MALLA_PERDIDO_REVES = "mallaPerdidoReves";

    public ProgressDialog mProgressDialog;
    private Map<String, Integer> puntosGanadosTotales = new HashMap<String, Integer>();
    private Map<String, Integer> puntosPerdidosTotales = new HashMap<String, Integer>();
    private Map<String, Integer> puntosGanadosSet1 = new HashMap<String, Integer>();
    private Map<String, Integer> puntosPerdidosSet1 = new HashMap<String, Integer>();
    private Map<String, Integer> puntosGanadosSet2 = new HashMap<String, Integer>();
    private Map<String, Integer> puntosPerdidosSet2 = new HashMap<String, Integer>();
    private Map<String, Integer> puntosGanadosSet3 = new HashMap<String, Integer>();
    private Map<String, Integer> puntosPerdidosSet3 = new HashMap<String, Integer>();
    private Map<String, Integer> puntosGanadosSet4 = new HashMap<String, Integer>();
    private Map<String, Integer> puntosPerdidosSet4 = new HashMap<String, Integer>();
    private Map<String, Integer> puntosGanadosSet5 = new HashMap<String, Integer>();
    private Map<String, Integer> puntosPerdidosSet5 = new HashMap<String, Integer>();
    private int totalPuntosGanados = 0;
    private int totalPuntosPerdidos = 0;
    private int totalPuntosGanadosSet1 = 0;
    private int totalPuntosPerdidosSet1 = 0;
    private int totalPuntosGanadosSet2 = 0;
    private int totalPuntosPerdidosSet2 = 0;
    private int totalPuntosGanadosSet3 = 0;
    private int totalPuntosPerdidosSet3 = 0;
    private int totalPuntosGanadosSet4 = 0;
    private int totalPuntosPerdidosSet4 = 0;
    private int totalPuntosGanadosSet5 = 0;
    private int totalPuntosPerdidosSet5 = 0;
    private float xDown;
    private float yDown;
    private float xUp;
    private float yUp;
    private int swAtack = 0;
    private int swCaida = 0;
    private String atack = "";
    private String caida = "";
    private String raqueta = "drive";
    private String mensaje = "";
    private boolean puntoGanado = false;
    private boolean puntoPerdido = false;

    private TextView txtNombre;
    private TextView txtPuntosJ1;
    private TextView txtPuntosJ2;
    private TextView txtRaqueta;
    private ImageButton tablero;
    private int setsGanados = 0;
    private int setsPerdidos = 0;
    private int setsTotales = 1;
    private int puntosGanadosPorSet = 0;
    private int puntosPerdidosPorSet = 0;
    private SetPartido set1;
    private SetPartido set2;
    private SetPartido set3;
    private SetPartido set4;
    private SetPartido set5;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partido);

        puntosGanadosTotales.put(ADENTRO_GANADO_DRIVE, 0);
        puntosGanadosTotales.put(ADENTRO_GANADO_REVES, 0);
        puntosGanadosTotales.put(AFUERA_GANADO_DRIVE, 0);
        puntosGanadosTotales.put(AFUERA_GANADO_REVES, 0);
        puntosGanadosTotales.put(MALLA_GANADO_DRIVE, 0);
        puntosGanadosTotales.put(MALLA_GANADO_REVES, 0);

        puntosPerdidosTotales.put(ADENTRO_PERDIDO_DRIVE, 0);
        puntosPerdidosTotales.put(ADENTRO_PERDIDO_REVES, 0);
        puntosPerdidosTotales.put(AFUERA_PERDIDO_DRIVE, 0);
        puntosPerdidosTotales.put(AFUERA_PERDIDO_REVES, 0);
        puntosPerdidosTotales.put(MALLA_PERDIDO_DRIVE, 0);
        puntosPerdidosTotales.put(MALLA_PERDIDO_REVES, 0);

        puntosGanadosSet1.put(ADENTRO_GANADO_DRIVE, 0);
        puntosGanadosSet1.put(ADENTRO_GANADO_REVES, 0);
        puntosGanadosSet1.put(AFUERA_GANADO_DRIVE, 0);
        puntosGanadosSet1.put(AFUERA_GANADO_REVES, 0);
        puntosGanadosSet1.put(MALLA_GANADO_DRIVE, 0);
        puntosGanadosSet1.put(MALLA_GANADO_REVES, 0);

        puntosPerdidosSet1.put(ADENTRO_PERDIDO_DRIVE, 0);
        puntosPerdidosSet1.put(ADENTRO_PERDIDO_REVES, 0);
        puntosPerdidosSet1.put(AFUERA_PERDIDO_DRIVE, 0);
        puntosPerdidosSet1.put(AFUERA_PERDIDO_REVES, 0);
        puntosPerdidosSet1.put(MALLA_PERDIDO_DRIVE, 0);
        puntosPerdidosSet1.put(MALLA_PERDIDO_REVES, 0);

        puntosGanadosSet2.put(ADENTRO_GANADO_DRIVE, 0);
        puntosGanadosSet2.put(ADENTRO_GANADO_REVES, 0);
        puntosGanadosSet2.put(AFUERA_GANADO_DRIVE, 0);
        puntosGanadosSet2.put(AFUERA_GANADO_REVES, 0);
        puntosGanadosSet2.put(MALLA_GANADO_DRIVE, 0);
        puntosGanadosSet2.put(MALLA_GANADO_REVES, 0);

        puntosPerdidosSet2.put(ADENTRO_PERDIDO_DRIVE, 0);
        puntosPerdidosSet2.put(ADENTRO_PERDIDO_REVES, 0);
        puntosPerdidosSet2.put(AFUERA_PERDIDO_DRIVE, 0);
        puntosPerdidosSet2.put(AFUERA_PERDIDO_REVES, 0);
        puntosPerdidosSet2.put(MALLA_PERDIDO_DRIVE, 0);
        puntosPerdidosSet2.put(MALLA_PERDIDO_REVES, 0);

        puntosGanadosSet3.put(ADENTRO_GANADO_DRIVE, 0);
        puntosGanadosSet3.put(ADENTRO_GANADO_REVES, 0);
        puntosGanadosSet3.put(AFUERA_GANADO_DRIVE, 0);
        puntosGanadosSet3.put(AFUERA_GANADO_REVES, 0);
        puntosGanadosSet3.put(MALLA_GANADO_DRIVE, 0);
        puntosGanadosSet3.put(MALLA_GANADO_REVES, 0);

        puntosPerdidosSet3.put(ADENTRO_PERDIDO_DRIVE, 0);
        puntosPerdidosSet3.put(ADENTRO_PERDIDO_REVES, 0);
        puntosPerdidosSet3.put(AFUERA_PERDIDO_DRIVE, 0);
        puntosPerdidosSet3.put(AFUERA_PERDIDO_REVES, 0);
        puntosPerdidosSet3.put(MALLA_PERDIDO_DRIVE, 0);
        puntosPerdidosSet3.put(MALLA_PERDIDO_REVES, 0);

        puntosGanadosSet4.put(ADENTRO_GANADO_DRIVE, 0);
        puntosGanadosSet4.put(ADENTRO_GANADO_REVES, 0);
        puntosGanadosSet4.put(AFUERA_GANADO_DRIVE, 0);
        puntosGanadosSet4.put(AFUERA_GANADO_REVES, 0);
        puntosGanadosSet4.put(MALLA_GANADO_DRIVE, 0);
        puntosGanadosSet4.put(MALLA_GANADO_REVES, 0);

        puntosPerdidosSet4.put(ADENTRO_PERDIDO_DRIVE, 0);
        puntosPerdidosSet4.put(ADENTRO_PERDIDO_REVES, 0);
        puntosPerdidosSet4.put(AFUERA_PERDIDO_DRIVE, 0);
        puntosPerdidosSet4.put(AFUERA_PERDIDO_REVES, 0);
        puntosPerdidosSet4.put(MALLA_PERDIDO_DRIVE, 0);
        puntosPerdidosSet4.put(MALLA_PERDIDO_REVES, 0);

        puntosGanadosSet5.put(ADENTRO_GANADO_DRIVE, 0);
        puntosGanadosSet5.put(ADENTRO_GANADO_REVES, 0);
        puntosGanadosSet5.put(AFUERA_GANADO_DRIVE, 0);
        puntosGanadosSet5.put(AFUERA_GANADO_REVES, 0);
        puntosGanadosSet5.put(MALLA_GANADO_DRIVE, 0);
        puntosGanadosSet5.put(MALLA_GANADO_REVES, 0);

        puntosPerdidosSet5.put(ADENTRO_PERDIDO_DRIVE, 0);
        puntosPerdidosSet5.put(ADENTRO_PERDIDO_REVES, 0);
        puntosPerdidosSet5.put(AFUERA_PERDIDO_DRIVE, 0);
        puntosPerdidosSet5.put(AFUERA_PERDIDO_REVES, 0);
        puntosPerdidosSet5.put(MALLA_PERDIDO_DRIVE, 0);
        puntosPerdidosSet5.put(MALLA_PERDIDO_REVES, 0);


        Intent intent = getIntent();
        ENTRENADOR = intent.getStringExtra("entrenador");
        JUGADOR = intent.getStringExtra("jugador");
        NOMBRE_JUGADOR = intent.getStringExtra("nombreJugador");
        NOMBRE_PARTIDO = intent.getStringExtra("nombrePartido");

        txtPuntosJ1 = findViewById(R.id.txtPuntosJ1);
        txtPuntosJ2 = findViewById(R.id.txtPuntosJ2);
        txtNombre = findViewById(R.id.txtNombre);
        tablero = findViewById(R.id.tablero);
        txtRaqueta = findViewById(R.id.txtRaqueta);

        txtNombre.setText(NOMBRE_PARTIDO);
        txtPuntosJ1.setText("J1: 0[0]");
        txtPuntosJ2.setText("J2: 0[0]");


        tablero.setOnTouchListener((v, event) -> {


            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                xDown = event.getX();
                yDown = event.getY();
                if(raqueta.equals("drive")){
                    txtRaqueta.setText("Drive");
                }else if (raqueta.equals("reves")){
                    txtRaqueta.setText("Reves");
                }
                agregarPuntoAtaque();
                swAtack = 1;
                swCaida = 0;
                Log.i("Tablero", "Down, X:" + xDown + ", Y:" + yDown);
                /*Point size = new Point();
                tablero.getDisplay().getSize(size);
                Log.e("Tamano", "X: " + tablero.getDrawable().getBounds().width() + "," + tablero.getWidth() + " Y: " + tablero.getDrawable().getBounds().height() + "," + tablero.getHeight());*/
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                xUp = event.getX();
                yUp = event.getY();
                agregarPuntoCaida();
                swCaida = 1;
                Log.i("Tablero", "Up, X:" + xUp + ", Y:" + yUp);
            }
            if (swAtack == 1 && swCaida == 1) {
                agregarPunto();
                atack = "";
                caida = "";
                mensaje = "";
                swAtack = 0;
                swCaida = 0;
            }
            return true;
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            elegirLadoRaqueta(view.getContext());
            /*Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();*/
        });

    }

    private void terminarPartido() {
        agregarPartido();
        Intent intent2 = new Intent(getApplicationContext(), EstadisticaActivity.class);
        intent2.putExtra("partido", NOMBRE_PARTIDO);
        intent2.putExtra("jugador", JUGADOR);
        intent2.putExtra("id", ID);
        intent2.putExtra("atras", 0);
        startActivity(intent2);
    }

    private void agregarPunto() {

        if (setsTotales < 6 && setsGanados != 3 && setsPerdidos != 3) {

            switch (atack + "," + caida) {

                case "AtaqueJugador1,CaidaJugador2": {
                    if (raqueta == "drive") {
                        validarPuntoSetAdentroGanadoDrive();
                    } else if (raqueta == "reves") {
                        validarPuntoSetAdentroGanadoReves();
                    }
                    puntoGanado = true;
                    break;
                }

                case "AtaqueJugador1,CaidaAfueraJugador2": {
                    if (raqueta == "drive") {
                        validarPuntoSetAfueraPerdidoDrive();
                    } else if (raqueta == "reves") {
                        validarPuntoSetAfueraPerdidoReves();
                    }
                    puntoPerdido = true;
                    break;
                }

                case "AtaqueJugador1,CaidaMalla": {
                    if (raqueta == "drive") {
                        validarPuntoSetMallaPerdidoDrive();
                    } else if (raqueta == "reves") {
                        validarPuntoSetMallaPerdidoReves();
                    }
                    puntoPerdido = true;
                    break;
                }

                case "AtaqueJugador2,CaidaJugador1": {
                    if (raqueta == "drive") {
                        validarPuntoSetAdentroPerdidoDrive();
                    } else if (raqueta == "reves") {
                        validarPuntoSetAdentroPerdidoReves();
                    }
                    puntoPerdido = true;
                    break;
                }

                case "AtaqueJugador2,CaidaAfueraJugador1": {
                    if (raqueta == "drive") {
                        validarPuntoSetAfueraGanadoDrive();
                    } else if (raqueta == "reves") {
                        validarPuntoSetAfueraGanadoReves();
                    }
                    puntoGanado = true;
                    break;
                }

                case "AtaqueJugador2,CaidaMalla": {
                    if (raqueta == "drive") {
                        validarPuntoSetMallaGanadoDrive();
                    } else if (raqueta == "reves") {
                        validarPuntoSetMallaGanadoReves();
                    }
                    puntoGanado = true;
                    break;
                }

            }

            if (!mensaje.equals("")) {
                Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();
            }

            definicionPuntoGanadoOPerdido();

            txtPuntosJ1.setText("J1: " + puntosGanadosPorSet + "[" + setsGanados + "]");
            txtPuntosJ2.setText("J2: " + puntosPerdidosPorSet + "[" + setsPerdidos + "]");

            if (setsGanados == 3 || setsPerdidos == 3) {
                terminarPartido();
            }

        } else {
            terminarPartido();
        }

    }

    private void validarPuntoSetAdentroGanadoDrive() {
        if (setsTotales == 1) {
            totalPuntosGanadosSet1++;
            puntosGanadosSet1.put(ADENTRO_GANADO_DRIVE, puntosGanadosSet1.get(ADENTRO_GANADO_DRIVE) + 1);
        } else if (setsTotales == 2) {
            totalPuntosGanadosSet2++;
            puntosGanadosSet2.put(ADENTRO_GANADO_DRIVE, puntosGanadosSet2.get(ADENTRO_GANADO_DRIVE) + 1);
        } else if (setsTotales == 3) {
            totalPuntosGanadosSet3++;
            puntosGanadosSet3.put(ADENTRO_GANADO_DRIVE, puntosGanadosSet3.get(ADENTRO_GANADO_DRIVE) + 1);
        } else if (setsTotales == 4) {
            totalPuntosGanadosSet4++;
            puntosGanadosSet4.put(ADENTRO_GANADO_DRIVE, puntosGanadosSet4.get(ADENTRO_GANADO_DRIVE) + 1);
        } else if (setsTotales == 5) {
            totalPuntosGanadosSet5++;
            puntosGanadosSet5.put(ADENTRO_GANADO_DRIVE, puntosGanadosSet5.get(ADENTRO_GANADO_DRIVE) + 1);
        }

        puntosGanadosTotales.put(ADENTRO_GANADO_DRIVE, puntosGanadosTotales.get(ADENTRO_GANADO_DRIVE) + 1);
        mensaje = "AtaqueJ1, CaidaJ2, Drive";
    }

    private void validarPuntoSetAdentroGanadoReves() {
        if (setsTotales == 1) {
            totalPuntosGanadosSet1++;
            puntosGanadosSet1.put(ADENTRO_GANADO_REVES, puntosGanadosSet1.get(ADENTRO_GANADO_REVES) + 1);
        } else if (setsTotales == 2) {
            totalPuntosGanadosSet2++;
            puntosGanadosSet2.put(ADENTRO_GANADO_REVES, puntosGanadosSet2.get(ADENTRO_GANADO_REVES) + 1);
        } else if (setsTotales == 3) {
            totalPuntosGanadosSet3++;
            puntosGanadosSet3.put(ADENTRO_GANADO_REVES, puntosGanadosSet3.get(ADENTRO_GANADO_REVES) + 1);
        } else if (setsTotales == 4) {
            totalPuntosGanadosSet4++;
            puntosGanadosSet4.put(ADENTRO_GANADO_REVES, puntosGanadosSet4.get(ADENTRO_GANADO_REVES) + 1);
        } else if (setsTotales == 5) {
            totalPuntosGanadosSet5++;
            puntosGanadosSet5.put(ADENTRO_GANADO_REVES, puntosGanadosSet5.get(ADENTRO_GANADO_REVES) + 1);
        }

        puntosGanadosTotales.put(ADENTRO_GANADO_REVES, puntosGanadosTotales.get(ADENTRO_GANADO_REVES) + 1);
        mensaje = "AtaqueJ1, CaidaJ2, Reves";
    }

    private void validarPuntoSetAfueraPerdidoDrive() {
        if (setsTotales == 1) {
            totalPuntosPerdidosSet1++;
            puntosPerdidosSet1.put(AFUERA_PERDIDO_DRIVE, puntosPerdidosSet1.get(AFUERA_PERDIDO_DRIVE) + 1);
        } else if (setsTotales == 2) {
            totalPuntosPerdidosSet2++;
            puntosPerdidosSet2.put(AFUERA_PERDIDO_DRIVE, puntosPerdidosSet2.get(AFUERA_PERDIDO_DRIVE) + 1);
        } else if (setsTotales == 3) {
            totalPuntosPerdidosSet3++;
            puntosPerdidosSet3.put(AFUERA_PERDIDO_DRIVE, puntosPerdidosSet3.get(AFUERA_PERDIDO_DRIVE) + 1);
        } else if (setsTotales == 4) {
            totalPuntosPerdidosSet4++;
            puntosPerdidosSet4.put(AFUERA_PERDIDO_DRIVE, puntosPerdidosSet4.get(AFUERA_PERDIDO_DRIVE) + 1);
        } else if (setsTotales == 5) {
            totalPuntosPerdidosSet5++;
            puntosPerdidosSet5.put(AFUERA_PERDIDO_DRIVE, puntosPerdidosSet5.get(AFUERA_PERDIDO_DRIVE) + 1);
        }

        puntosPerdidosTotales.put(AFUERA_PERDIDO_DRIVE, puntosPerdidosTotales.get(AFUERA_PERDIDO_DRIVE) + 1);
        mensaje = "AtaqueJ1, CaidaAfueraJ2, Drive";
    }

    private void validarPuntoSetAfueraPerdidoReves() {
        if (setsTotales == 1) {
            totalPuntosPerdidosSet1++;
            puntosPerdidosSet1.put(AFUERA_PERDIDO_REVES, puntosPerdidosSet1.get(AFUERA_PERDIDO_REVES) + 1);
        } else if (setsTotales == 2) {
            totalPuntosPerdidosSet2++;
            puntosPerdidosSet2.put(AFUERA_PERDIDO_REVES, puntosPerdidosSet2.get(AFUERA_PERDIDO_REVES) + 1);
        } else if (setsTotales == 3) {
            totalPuntosPerdidosSet3++;
            puntosPerdidosSet3.put(AFUERA_PERDIDO_REVES, puntosPerdidosSet3.get(AFUERA_PERDIDO_REVES) + 1);
        } else if (setsTotales == 4) {
            totalPuntosPerdidosSet4++;
            puntosPerdidosSet4.put(AFUERA_PERDIDO_REVES, puntosPerdidosSet4.get(AFUERA_PERDIDO_REVES) + 1);
        } else if (setsTotales == 5) {
            totalPuntosPerdidosSet5++;
            puntosPerdidosSet5.put(AFUERA_PERDIDO_REVES, puntosPerdidosSet5.get(AFUERA_PERDIDO_REVES) + 1);
        }

        puntosPerdidosTotales.put(AFUERA_PERDIDO_REVES, puntosPerdidosTotales.get(AFUERA_PERDIDO_REVES) + 1);
        mensaje = "AtaqueJ1, CaidaAfueraJ2, Reves";
    }

    private void validarPuntoSetMallaPerdidoDrive() {
        if (setsTotales == 1) {
            totalPuntosPerdidosSet1++;
            puntosPerdidosSet1.put(MALLA_PERDIDO_DRIVE, puntosPerdidosSet1.get(MALLA_PERDIDO_DRIVE) + 1);
        } else if (setsTotales == 2) {
            totalPuntosPerdidosSet2++;
            puntosPerdidosSet2.put(MALLA_PERDIDO_DRIVE, puntosPerdidosSet2.get(MALLA_PERDIDO_DRIVE) + 1);
        } else if (setsTotales == 3) {
            totalPuntosPerdidosSet3++;
            puntosPerdidosSet3.put(MALLA_PERDIDO_DRIVE, puntosPerdidosSet3.get(MALLA_PERDIDO_DRIVE) + 1);
        } else if (setsTotales == 4) {
            totalPuntosPerdidosSet4++;
            puntosPerdidosSet4.put(MALLA_PERDIDO_DRIVE, puntosPerdidosSet4.get(MALLA_PERDIDO_DRIVE) + 1);
        } else if (setsTotales == 5) {
            totalPuntosPerdidosSet5++;
            puntosPerdidosSet5.put(MALLA_PERDIDO_DRIVE, puntosPerdidosSet5.get(MALLA_PERDIDO_DRIVE) + 1);
        }

        puntosPerdidosTotales.put(MALLA_PERDIDO_DRIVE, puntosPerdidosTotales.get(MALLA_PERDIDO_DRIVE) + 1);
        mensaje = "AtaqueJ1, CaidaMalla, Drive";
    }

    private void validarPuntoSetMallaPerdidoReves() {
        if (setsTotales == 1) {
            totalPuntosPerdidosSet1++;
            puntosPerdidosSet1.put(MALLA_PERDIDO_REVES, puntosPerdidosSet1.get(MALLA_PERDIDO_REVES) + 1);
        } else if (setsTotales == 2) {
            totalPuntosPerdidosSet2++;
            puntosPerdidosSet2.put(MALLA_PERDIDO_REVES, puntosPerdidosSet2.get(MALLA_PERDIDO_REVES) + 1);
        } else if (setsTotales == 3) {
            totalPuntosPerdidosSet3++;
            puntosPerdidosSet3.put(MALLA_PERDIDO_REVES, puntosPerdidosSet3.get(MALLA_PERDIDO_REVES) + 1);
        } else if (setsTotales == 4) {
            totalPuntosPerdidosSet4++;
            puntosPerdidosSet4.put(MALLA_PERDIDO_REVES, puntosPerdidosSet4.get(MALLA_PERDIDO_REVES) + 1);
        } else if (setsTotales == 5) {
            totalPuntosPerdidosSet5++;
            puntosPerdidosSet5.put(MALLA_PERDIDO_REVES, puntosPerdidosSet5.get(MALLA_PERDIDO_REVES) + 1);
        }

        puntosPerdidosTotales.put(MALLA_PERDIDO_REVES, puntosPerdidosTotales.get(MALLA_PERDIDO_REVES) + 1);
        mensaje = "AtaqueJ1, CaidaMalla, Reves";
    }

    private void validarPuntoSetAdentroPerdidoDrive() {
        if (setsTotales == 1) {
            totalPuntosPerdidosSet1++;
            puntosPerdidosSet1.put(ADENTRO_PERDIDO_DRIVE, puntosPerdidosSet1.get(ADENTRO_PERDIDO_DRIVE) + 1);
        } else if (setsTotales == 2) {
            totalPuntosPerdidosSet2++;
            puntosPerdidosSet2.put(ADENTRO_PERDIDO_DRIVE, puntosPerdidosSet2.get(ADENTRO_PERDIDO_DRIVE) + 1);
        } else if (setsTotales == 3) {
            totalPuntosPerdidosSet3++;
            puntosPerdidosSet3.put(ADENTRO_PERDIDO_DRIVE, puntosPerdidosSet3.get(ADENTRO_PERDIDO_DRIVE) + 1);
        } else if (setsTotales == 4) {
            totalPuntosPerdidosSet4++;
            puntosPerdidosSet4.put(ADENTRO_PERDIDO_DRIVE, puntosPerdidosSet4.get(ADENTRO_PERDIDO_DRIVE) + 1);
        } else if (setsTotales == 5) {
            totalPuntosPerdidosSet5++;
            puntosPerdidosSet5.put(ADENTRO_PERDIDO_DRIVE, puntosPerdidosSet5.get(ADENTRO_PERDIDO_DRIVE) + 1);
        }

        puntosPerdidosTotales.put(ADENTRO_PERDIDO_DRIVE, puntosPerdidosTotales.get(ADENTRO_PERDIDO_DRIVE) + 1);
        mensaje = "AtaqueJ2, CaidaJ1, Drive";
    }

    private void validarPuntoSetAdentroPerdidoReves() {
        if (setsTotales == 1) {
            totalPuntosPerdidosSet1++;
            puntosPerdidosSet1.put(ADENTRO_PERDIDO_REVES, puntosPerdidosSet1.get(ADENTRO_PERDIDO_REVES) + 1);
        } else if (setsTotales == 2) {
            totalPuntosPerdidosSet2++;
            puntosPerdidosSet2.put(ADENTRO_PERDIDO_REVES, puntosPerdidosSet2.get(ADENTRO_PERDIDO_REVES) + 1);
        } else if (setsTotales == 3) {
            totalPuntosPerdidosSet3++;
            puntosPerdidosSet3.put(ADENTRO_PERDIDO_REVES, puntosPerdidosSet3.get(ADENTRO_PERDIDO_REVES) + 1);
        } else if (setsTotales == 4) {
            totalPuntosPerdidosSet4++;
            puntosPerdidosSet4.put(ADENTRO_PERDIDO_REVES, puntosPerdidosSet4.get(ADENTRO_PERDIDO_REVES) + 1);
        } else if (setsTotales == 5) {
            totalPuntosPerdidosSet5++;
            puntosPerdidosSet5.put(ADENTRO_PERDIDO_REVES, puntosPerdidosSet5.get(ADENTRO_PERDIDO_REVES) + 1);
        }

        puntosPerdidosTotales.put(ADENTRO_PERDIDO_REVES, puntosPerdidosTotales.get(ADENTRO_PERDIDO_REVES) + 1);
        mensaje = "AtaqueJ2, CaidaJ1, Reves";
    }

    private void validarPuntoSetAfueraGanadoDrive() {
        if (setsTotales == 1) {
            totalPuntosGanadosSet1++;
            puntosGanadosSet1.put(AFUERA_GANADO_DRIVE, puntosGanadosSet1.get(AFUERA_GANADO_DRIVE) + 1);
        } else if (setsTotales == 2) {
            totalPuntosGanadosSet2++;
            puntosGanadosSet2.put(AFUERA_GANADO_DRIVE, puntosGanadosSet2.get(AFUERA_GANADO_DRIVE) + 1);
        } else if (setsTotales == 3) {
            totalPuntosGanadosSet3++;
            puntosGanadosSet3.put(AFUERA_GANADO_DRIVE, puntosGanadosSet3.get(AFUERA_GANADO_DRIVE) + 1);
        } else if (setsTotales == 4) {
            totalPuntosGanadosSet4++;
            puntosGanadosSet4.put(AFUERA_GANADO_DRIVE, puntosGanadosSet4.get(AFUERA_GANADO_DRIVE) + 1);
        } else if (setsTotales == 5) {
            totalPuntosGanadosSet5++;
            puntosGanadosSet5.put(AFUERA_GANADO_DRIVE, puntosGanadosSet5.get(AFUERA_GANADO_DRIVE) + 1);
        }

        puntosGanadosTotales.put(AFUERA_GANADO_DRIVE, puntosGanadosTotales.get(AFUERA_GANADO_DRIVE) + 1);
        mensaje = "AtaqueJ2, CaidaAfueraJ1, Drive";
    }

    private void validarPuntoSetAfueraGanadoReves() {
        if (setsTotales == 1) {
            totalPuntosGanadosSet1++;
            puntosGanadosSet1.put(AFUERA_GANADO_REVES, puntosGanadosSet1.get(AFUERA_GANADO_REVES) + 1);
        } else if (setsTotales == 2) {
            totalPuntosGanadosSet2++;
            puntosGanadosSet2.put(AFUERA_GANADO_REVES, puntosGanadosSet2.get(AFUERA_GANADO_REVES) + 1);
        } else if (setsTotales == 3) {
            totalPuntosGanadosSet3++;
            puntosGanadosSet3.put(AFUERA_GANADO_REVES, puntosGanadosSet3.get(AFUERA_GANADO_REVES) + 1);
        } else if (setsTotales == 4) {
            totalPuntosGanadosSet4++;
            puntosGanadosSet4.put(AFUERA_GANADO_REVES, puntosGanadosSet4.get(AFUERA_GANADO_REVES) + 1);
        } else if (setsTotales == 5) {
            totalPuntosGanadosSet5++;
            puntosGanadosSet5.put(AFUERA_GANADO_REVES, puntosGanadosSet5.get(AFUERA_GANADO_REVES) + 1);
        }

        puntosGanadosTotales.put(AFUERA_GANADO_REVES, puntosGanadosTotales.get(AFUERA_GANADO_REVES) + 1);
        mensaje = "AtaqueJ2, CaidaAfueraJ1, Reves";
    }

    private void validarPuntoSetMallaGanadoDrive() {
        if (setsTotales == 1) {
            totalPuntosGanadosSet1++;
            puntosGanadosSet1.put(MALLA_GANADO_DRIVE, puntosGanadosSet1.get(MALLA_GANADO_DRIVE) + 1);
        } else if (setsTotales == 2) {
            totalPuntosGanadosSet2++;
            puntosGanadosSet2.put(MALLA_GANADO_DRIVE, puntosGanadosSet2.get(MALLA_GANADO_DRIVE) + 1);
        } else if (setsTotales == 3) {
            totalPuntosGanadosSet3++;
            puntosGanadosSet3.put(MALLA_GANADO_DRIVE, puntosGanadosSet3.get(MALLA_GANADO_DRIVE) + 1);
        } else if (setsTotales == 4) {
            totalPuntosGanadosSet4++;
            puntosGanadosSet4.put(MALLA_GANADO_DRIVE, puntosGanadosSet4.get(MALLA_GANADO_DRIVE) + 1);
        } else if (setsTotales == 5) {
            totalPuntosGanadosSet5++;
            puntosGanadosSet5.put(MALLA_GANADO_DRIVE, puntosGanadosSet5.get(MALLA_GANADO_DRIVE) + 1);
        }

        puntosGanadosTotales.put(MALLA_GANADO_DRIVE, puntosGanadosTotales.get(MALLA_GANADO_DRIVE) + 1);
        mensaje = "AtaqueJ2, CaidaMalla, Drive";
    }

    private void validarPuntoSetMallaGanadoReves() {
        if (setsTotales == 1) {
            totalPuntosGanadosSet1++;
            puntosGanadosSet1.put(MALLA_GANADO_REVES, puntosGanadosSet1.get(MALLA_GANADO_REVES) + 1);
        } else if (setsTotales == 2) {
            totalPuntosGanadosSet2++;
            puntosGanadosSet2.put(MALLA_GANADO_REVES, puntosGanadosSet2.get(MALLA_GANADO_REVES) + 1);
        } else if (setsTotales == 3) {
            totalPuntosGanadosSet3++;
            puntosGanadosSet3.put(MALLA_GANADO_REVES, puntosGanadosSet3.get(MALLA_GANADO_REVES) + 1);
        } else if (setsTotales == 4) {
            totalPuntosGanadosSet4++;
            puntosGanadosSet4.put(MALLA_GANADO_REVES, puntosGanadosSet4.get(MALLA_GANADO_REVES) + 1);
        } else if (setsTotales == 5) {
            totalPuntosGanadosSet5++;
            puntosGanadosSet5.put(MALLA_GANADO_REVES, puntosGanadosSet5.get(MALLA_GANADO_REVES) + 1);
        }

        puntosGanadosTotales.put(MALLA_GANADO_REVES, puntosGanadosTotales.get(MALLA_GANADO_REVES) + 1);
        mensaje = "AtaqueJ2, CaidaMalla, Reves";
    }

    private void definicionPuntoGanadoOPerdido() {
        if (puntoGanado) {
            puntosGanadosPorSet++;
            totalPuntosGanados++;
            if ((puntosGanadosPorSet >= 11) && (puntosPerdidosPorSet <= puntosGanadosPorSet - 2)) {
                setsGanados++;
                setsTotales++;
                puntosGanadosPorSet = 0;
                puntosPerdidosPorSet = 0;
                Toast.makeText(getApplicationContext(), "Gana el set el jugador 1"
                        + "\n" + totalPuntosGanadosSet1 + "-" + totalPuntosPerdidosSet1
                        + "\n" + totalPuntosGanadosSet2 + "-" + totalPuntosPerdidosSet2
                        + "\n" + totalPuntosGanadosSet3 + "-" + totalPuntosPerdidosSet3
                        + "\n" + totalPuntosGanadosSet4 + "-" + totalPuntosPerdidosSet4
                        + "\n" + totalPuntosGanadosSet5 + "-" + totalPuntosPerdidosSet5, Toast.LENGTH_LONG).show();
            }
            puntoGanado = false;
        } else if (puntoPerdido) {
            puntosPerdidosPorSet++;
            totalPuntosPerdidos++;
            if ((puntosPerdidosPorSet >= 11) && (puntosGanadosPorSet <= puntosPerdidosPorSet - 2)) {
                setsPerdidos++;
                setsTotales++;
                puntosGanadosPorSet = 0;
                puntosPerdidosPorSet = 0;
                Toast.makeText(getApplicationContext(), "Gana el set el jugador 2"
                        + "\n" + totalPuntosGanadosSet1 + "-" + totalPuntosPerdidosSet1
                        + "\n" + totalPuntosGanadosSet2 + "-" + totalPuntosPerdidosSet2
                        + "\n" + totalPuntosGanadosSet3 + "-" + totalPuntosPerdidosSet3
                        + "\n" + totalPuntosGanadosSet4 + "-" + totalPuntosPerdidosSet4
                        + "\n" + totalPuntosGanadosSet5 + "-" + totalPuntosPerdidosSet5, Toast.LENGTH_LONG).show();
            }
            puntoPerdido = false;
        }
    }


    private void agregarPuntoAtaque() {

        boolean jugador1 = false;

        if (xDown >= 220 && xDown <= 530 && yDown >= 890 && yDown <= 1400) {
            Log.i("Tablero", "primer rectangulo");
            atack = "AtaqueJugador1";
        } else if (xDown > 530 && xDown <= 840 && yDown >= 890 && yDown <= 1400) {
            Log.i("Tablero", "segundo rectangulo");
            atack = "AtaqueJugador1";
        } else if (xDown > 530 && xDown <= 840 && yDown >= 240 && yDown <= 750) {
            Log.i("Tablero", "Tercero rectangulo");
            atack = "AtaqueJugador2";
        } else if (xDown >= 220 && xDown <= 530 && yDown >= 240 && yDown <= 750) {
            Log.i("Tablero", "cuarto rectangulo");
            atack = "AtaqueJugador2";
        }

    }

    private void agregarPuntoCaida() {

        if (xUp >= 220 && xUp <= 530 && yUp >= 890 && yUp <= 1370) {
            Log.i("Tablero", "primer rectangulo");
            caida = "CaidaJugador1";
        } else if (xUp > 530 && xUp <= 840 && yUp >= 890 && yUp <= 1370) {
            Log.i("Tablero", "segundo rectangulo");
            caida = "CaidaJugador1";
        } else if (xUp > 530 && xUp <= 840 && yUp >= 240 && yUp <= 750) {
            Log.i("Tablero", "Tercero rectangulo");
            caida = "CaidaJugador2";
        } else if (xUp >= 220 && xUp <= 530 && yUp >= 240 && yUp <= 750) {
            Log.i("Tablero", "cuarto rectangulo");
            caida = "CaidaJugador2";

        } else if (xUp >= 0 && yUp > 1400) {
            Log.e("Tablero", "Afuera abajo");
            caida = "CaidaAfueraJugador1";
        } else if (xUp >= 0 && xUp < 240 && yUp >= 890 && yUp <= 1400) {
            Log.e("Tablero", "Afuera abajo izquierda");
            caida = "CaidaAfueraJugador1";
        } else if (xUp > 840 && yUp >= 890 && yUp <= 1400) {
            Log.e("Tablero", "Afuera abajo derecha");
            caida = "CaidaAfueraJugador1";

        } else if (xUp >= 0 && yUp < 240) {
            Log.e("Tablero", "Afuera arriba");
            caida = "CaidaAfueraJugador2";
        } else if (xUp >= 0 && xUp < 240 && yUp >= 240 && yUp <= 750) {
            Log.e("Tablero", "Afuera arriba izquierda ");
            caida = "CaidaAfueraJugador2";
        } else if (xUp > 840 && yUp >= 240 && yUp <= 750) {
            Log.e("Tablero", "Afuera arriba derecha");
            caida = "CaidaAfueraJugador2";

        } else if (xUp >= 0 && yUp > 750 && yUp < 890) {
            caida = "CaidaMalla";
        }

    }


    private void agregarPartido() {

        showProgressDialog();
        String idEstadistica = UUID.randomUUID().toString();


        set1 = new SetPartido(idEstadistica,
                1,
                totalPuntosGanadosSet1,
                totalPuntosPerdidosSet1,
                puntosGanadosSet1.get(ADENTRO_GANADO_DRIVE),
                puntosGanadosSet1.get(ADENTRO_GANADO_REVES),
                puntosGanadosSet1.get(AFUERA_GANADO_DRIVE),
                puntosGanadosSet1.get(AFUERA_GANADO_REVES),
                puntosGanadosSet1.get(MALLA_GANADO_DRIVE),
                puntosGanadosSet1.get(MALLA_GANADO_REVES),
                puntosPerdidosSet1.get(ADENTRO_PERDIDO_DRIVE),
                puntosPerdidosSet1.get(ADENTRO_PERDIDO_REVES),
                puntosPerdidosSet1.get(AFUERA_PERDIDO_DRIVE),
                puntosPerdidosSet1.get(AFUERA_PERDIDO_REVES),
                puntosPerdidosSet1.get(MALLA_PERDIDO_DRIVE),
                puntosPerdidosSet1.get(MALLA_PERDIDO_REVES)
        );

        set2 = new SetPartido(idEstadistica,
                2,
                totalPuntosGanadosSet2,
                totalPuntosPerdidosSet2,
                puntosGanadosSet2.get(ADENTRO_GANADO_DRIVE),
                puntosGanadosSet2.get(ADENTRO_GANADO_REVES),
                puntosGanadosSet2.get(AFUERA_GANADO_DRIVE),
                puntosGanadosSet2.get(AFUERA_GANADO_REVES),
                puntosGanadosSet2.get(MALLA_GANADO_DRIVE),
                puntosGanadosSet2.get(MALLA_GANADO_REVES),
                puntosPerdidosSet2.get(ADENTRO_PERDIDO_DRIVE),
                puntosPerdidosSet2.get(ADENTRO_PERDIDO_REVES),
                puntosPerdidosSet2.get(AFUERA_PERDIDO_DRIVE),
                puntosPerdidosSet2.get(AFUERA_PERDIDO_REVES),
                puntosPerdidosSet2.get(MALLA_PERDIDO_DRIVE),
                puntosPerdidosSet2.get(MALLA_PERDIDO_REVES)
        );

        set3 = new SetPartido(idEstadistica,
                3,
                totalPuntosGanadosSet3,
                totalPuntosPerdidosSet3,
                puntosGanadosSet3.get(ADENTRO_GANADO_DRIVE),
                puntosGanadosSet3.get(ADENTRO_GANADO_REVES),
                puntosGanadosSet3.get(AFUERA_GANADO_DRIVE),
                puntosGanadosSet3.get(AFUERA_GANADO_REVES),
                puntosGanadosSet3.get(MALLA_GANADO_DRIVE),
                puntosGanadosSet3.get(MALLA_GANADO_REVES),
                puntosPerdidosSet3.get(ADENTRO_PERDIDO_DRIVE),
                puntosPerdidosSet3.get(ADENTRO_PERDIDO_REVES),
                puntosPerdidosSet3.get(AFUERA_PERDIDO_DRIVE),
                puntosPerdidosSet3.get(AFUERA_PERDIDO_REVES),
                puntosPerdidosSet3.get(MALLA_PERDIDO_DRIVE),
                puntosPerdidosSet3.get(MALLA_PERDIDO_REVES)
        );

        set4 = new SetPartido(idEstadistica,
                4,
                totalPuntosGanadosSet4,
                totalPuntosPerdidosSet4,
                puntosGanadosSet4.get(ADENTRO_GANADO_DRIVE),
                puntosGanadosSet4.get(ADENTRO_GANADO_REVES),
                puntosGanadosSet4.get(AFUERA_GANADO_DRIVE),
                puntosGanadosSet4.get(AFUERA_GANADO_REVES),
                puntosGanadosSet4.get(MALLA_GANADO_DRIVE),
                puntosGanadosSet4.get(MALLA_GANADO_REVES),
                puntosPerdidosSet4.get(ADENTRO_PERDIDO_DRIVE),
                puntosPerdidosSet4.get(ADENTRO_PERDIDO_REVES),
                puntosPerdidosSet4.get(AFUERA_PERDIDO_DRIVE),
                puntosPerdidosSet4.get(AFUERA_PERDIDO_REVES),
                puntosPerdidosSet4.get(MALLA_PERDIDO_DRIVE),
                puntosPerdidosSet4.get(MALLA_PERDIDO_REVES)
        );


        set5 = new SetPartido(idEstadistica,
                5,
                totalPuntosGanadosSet5,
                totalPuntosPerdidosSet5,
                puntosGanadosSet5.get(ADENTRO_GANADO_DRIVE),
                puntosGanadosSet5.get(ADENTRO_GANADO_REVES),
                puntosGanadosSet5.get(AFUERA_GANADO_DRIVE),
                puntosGanadosSet5.get(AFUERA_GANADO_REVES),
                puntosGanadosSet5.get(MALLA_GANADO_DRIVE),
                puntosGanadosSet5.get(MALLA_GANADO_REVES),
                puntosPerdidosSet5.get(ADENTRO_PERDIDO_DRIVE),
                puntosPerdidosSet5.get(ADENTRO_PERDIDO_REVES),
                puntosPerdidosSet5.get(AFUERA_PERDIDO_DRIVE),
                puntosPerdidosSet5.get(AFUERA_PERDIDO_REVES),
                puntosPerdidosSet5.get(MALLA_PERDIDO_DRIVE),
                puntosPerdidosSet5.get(MALLA_PERDIDO_REVES)
        );

        Estadistica estadistica = new Estadistica(
                idEstadistica,
                totalPuntosGanados,
                totalPuntosPerdidos,
                set1,
                set2,
                set3,
                set4,
                set5
        );

        String nombres = NOMBRE_PARTIDO;
        String estaditicas = estadistica.getId();
        String fecha = Calendar.getInstance().getTime().toString();

        String duracion = "Duro: " + 0;
        ID = estadistica.getId();

        Log.i("estadistica", ID);

        Partido partido = new Partido(estaditicas, nombres, fecha, duracion, JUGADOR);

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);

        SQLiteDatabase bd = admin.getWritableDatabase();

        // los inserto en la base de datos
        bd.insert(Contratos.EstadisticaEntidad.TABLE_NAME, null, estadistica.toContentValues());
        bd.insert(Contratos.SetEntidad.TABLE_NAME, null, set1.toContentValues());
        bd.insert(Contratos.SetEntidad.TABLE_NAME, null, set2.toContentValues());
        bd.insert(Contratos.SetEntidad.TABLE_NAME, null, set3.toContentValues());
        bd.insert(Contratos.SetEntidad.TABLE_NAME, null, set4.toContentValues());
        bd.insert(Contratos.SetEntidad.TABLE_NAME, null, set5.toContentValues());
        bd.insert(Contratos.PartidoEntidad.TABLE_NAME, null, partido.toContentValues());

        bd.close();

        Toast.makeText(this, "Datos del partido guardados ", Toast.LENGTH_SHORT).show();


        hideProgressDialog();

    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Cargando");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }


    }

    private void elegirLadoRaqueta(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Seleccione lado de raqueta");
        builder.setIcon(R.drawable.ping_pong_raqu);
        builder.setPositiveButton("reves", (dialog, which) -> raqueta = "reves");
        builder.setNegativeButton("drive", (dialog, which) -> raqueta = "drive");


        AlertDialog dialog = builder.create();

        /*Button negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);

        negativeButton.setTextColor(Color.parseColor("#CD0000"));
        positiveButton.setTextColor(Color.parseColor("#000000"));*/

        dialog.show();
    }

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.partido, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.cerrar:
                terminarPartido();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
