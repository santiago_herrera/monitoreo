package co.edu.poli.monitoreo.actividades;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import co.edu.poli.monitoreo.R;
import co.edu.poli.monitoreo.modelo.Jugador;
import co.edu.poli.monitoreo.persistencia.AdminSQLiteOpenHelper;

public class Estadisticas extends AppCompatActivity {


    private static final int NUMERO = 1;
    private ArrayAdapter<String> adaptador;
    ListView lista;
    List<Jugador> jugadores;
    Button btnCancelar;

    private static String ENTRENADOR = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estadisticas);

        Intent intent = getIntent();
        ENTRENADOR = intent.getStringExtra("entrenador");

        lista = findViewById(R.id.listaJugadores);
        btnCancelar = findViewById(R.id.btnCancelar);
        adaptador = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        consulta();
        lista.setOnItemClickListener((parent, view, position, id) -> {

            Intent intent1 = new Intent(getApplicationContext(), Partidos.class);
            intent1.putExtra("jugador", jugadores.get(position).getId());
            startActivity(intent1);

        });

        btnCancelar.setOnClickListener(v -> finish());

    }

    public void consulta() {
        adaptador.clear();
        jugadores = new ArrayList<>();


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);

        SQLiteDatabase bd = admin.getWritableDatabase();
        String[] args = new String[]{ENTRENADOR};
        Cursor fila = bd.rawQuery("SELECT * FROM jugadores WHERE entrenador =?", args);


        if (fila.moveToFirst()) {
            do {
                Jugador jugador = new Jugador(
                        fila.getString(0),
                        fila.getString(1),
                        fila.getString(2),
                        fila.getString(3),
                        fila.getString(4),
                        fila.getString(5),
                        fila.getString(6)
                );

                jugadores.add(jugador);
                agregarLista(jugador.getNombres() + " " + jugador.getApellidos());
            } while (fila.moveToNext());
        }

        fila.close();
        bd.close();

    }

    private void agregarLista(String nombreCompleato) {
        adaptador.add(nombreCompleato);
        lista.setAdapter(adaptador);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == NUMERO) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
//                consulta();
            }
        }
    }
}
