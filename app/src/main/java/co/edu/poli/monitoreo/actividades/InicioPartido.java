package co.edu.poli.monitoreo.actividades;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.edu.poli.monitoreo.R;
import co.edu.poli.monitoreo.modelo.Jugador;
import co.edu.poli.monitoreo.persistencia.AdminSQLiteOpenHelper;

public class InicioPartido extends AppCompatActivity {


    private static String NOMBRE = "";
    private static String ENTRENADOR = "";
    private static String JUGADOR = "";
    EditText edtNombre;
    Spinner jugador1;
    List<Jugador> list;
    Button btnCancelar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_partido);

        Intent intent = getIntent();
        ENTRENADOR = intent.getStringExtra("entrenador");

        jugador1 = findViewById(R.id.spJugador1);
        Button btninicar = findViewById(R.id.btnIniciar);
        edtNombre = findViewById(R.id.edtNombre);
        btnCancelar = findViewById(R.id.btnCancelar);

        List<String> labels = consulta();

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, labels);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        jugador1.setAdapter(dataAdapter);

        jugador1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                JUGADOR = list.get(position).getId();
                NOMBRE = list.get(position).getNombres();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btninicar.setOnClickListener(v -> partido());
        btnCancelar.setOnClickListener(v -> finish());

    }

    private void partido() {
        if (!validateForm()) {
            return;
        }

        Intent intent = new Intent(getApplicationContext(), PartidoActivity.class);
        intent.putExtra("nombrePartido", edtNombre.getText().toString());
        intent.putExtra("jugador", JUGADOR);
        intent.putExtra("nombreJugador", NOMBRE);
        intent.putExtra("entrenador", ENTRENADOR);
        startActivity(intent);
    }


    public List<String> consulta() {
        list = new ArrayList<>();
        List<String> lista = new ArrayList<>();


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this);

        SQLiteDatabase bd = admin.getWritableDatabase();
        String[] args = new String[]{ENTRENADOR};
        Cursor cursor = bd.rawQuery("SELECT * FROM jugadores WHERE entrenador =?", args);

        if (cursor.moveToFirst()) {
            do {
                Jugador jugador = new Jugador(
                        cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6)
                );


                list.add(jugador);//adding 2nd column data
                lista.add(jugador.getNombres() + " " + jugador.getApellidos());//adding 2nd column data
            } while (cursor.moveToNext());
        }


        cursor.close();
        bd.close();

        return lista;

    }

    private boolean validateForm() {
        boolean valid = true;


        String nombres = edtNombre.getText().toString();
        if (TextUtils.isEmpty(nombres)) {
            edtNombre.setError("Requerido.");
            valid = false;
        } else {
            edtNombre.setError(null);
        }

        if (list.size() == 0 ) {
            Toast.makeText(this, "No hay jugadores ", Toast.LENGTH_SHORT).show();
            valid = false;
        }

        return valid;
    }

}
