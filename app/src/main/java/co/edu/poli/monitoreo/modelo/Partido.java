package co.edu.poli.monitoreo.modelo;

import android.content.ContentValues;

import co.edu.poli.monitoreo.persistencia.Contratos;

public class Partido {
    private Integer id;
    private String estadistica;
    private String nombre;
    private String fecha;
    private String duracion;
    private String jugador;

    public Partido(Integer id, String estadistica, String nombre, String fecha, String duracion, String jugador) {
        this.id = id;
        this.estadistica = estadistica;
        this.nombre = nombre;
        this.fecha = fecha;
        this.duracion = duracion;
        this.jugador = jugador;
    }

    public Partido(String estadistica, String nombre, String fecha, String duracion, String jugador) {
        this.estadistica = estadistica;
        this.nombre = nombre;
        this.fecha = fecha;
        this.duracion = duracion;
        this.jugador = jugador;
    }

    public Integer getId() {
        return id;
    }

    public String getEstadistica() {
        return estadistica;
    }

    public String getNombre() {
        return nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public String getDuracion() {
        return duracion;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(Contratos.PartidoEntidad.ESTADISTICA, estadistica);
        values.put(Contratos.PartidoEntidad.NOMBRE, nombre);
        values.put(Contratos.PartidoEntidad.FECHA, fecha);
        values.put(Contratos.PartidoEntidad.DURACION, duracion);
        values.put(Contratos.PartidoEntidad.JUGADOR, jugador);
        return values;
    }

    public String getJugador() {
        return jugador;
    }
}
