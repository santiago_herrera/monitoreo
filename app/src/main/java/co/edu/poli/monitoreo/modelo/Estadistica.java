package co.edu.poli.monitoreo.modelo;

import android.content.ContentValues;

import java.util.UUID;

import co.edu.poli.monitoreo.persistencia.Contratos;

public class Estadistica {
    private String id;
    private Integer ganados;
    private Integer perdidos;
    private SetPartido setPartido1;
    private SetPartido setPartido2;
    private SetPartido setPartido3;
    private SetPartido setPartido4;
    private SetPartido setPartido5;

    public Estadistica(String id, Integer ganados, Integer perdidos, SetPartido setPartido1, SetPartido setPartido2, SetPartido setPartido3, SetPartido setPartido4, SetPartido setPartido5) {
        this.id = id;
        this.ganados = ganados;
        this.perdidos = perdidos;
        this.setPartido1 = setPartido1;
        this.setPartido2 = setPartido2;
        this.setPartido3 = setPartido3;
        this.setPartido4 = setPartido4;
        this.setPartido5 = setPartido5;
    }

    public Estadistica(Integer ganados, Integer perdidos, SetPartido setPartido1, SetPartido setPartido2, SetPartido setPartido3, SetPartido setPartido4, SetPartido setPartido5) {
        this.id = UUID.randomUUID().toString();
        this.ganados = ganados;
        this.perdidos = perdidos;
        this.setPartido1 = setPartido1;
        this.setPartido2 = setPartido2;
        this.setPartido3 = setPartido3;
        this.setPartido4 = setPartido4;
        this.setPartido5 = setPartido5;
    }

    public String getId() {
        return id;
    }

    public Integer getGanados() {
        return ganados;
    }

    public Integer getPerdidos() {
        return perdidos;
    }

    public SetPartido getSetPartido1() {
        return setPartido1;
    }

    public SetPartido getSetPartido2() {
        return setPartido2;
    }

    public SetPartido getSetPartido3() {
        return setPartido3;
    }

    public SetPartido getSetPartido4() {
        return setPartido4;
    }

    public SetPartido getSetPartido5() {
        return setPartido5;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(Contratos.EstadisticaEntidad.ID, id);
        values.put(Contratos.EstadisticaEntidad.GANADOS, ganados);
        values.put(Contratos.EstadisticaEntidad.PERDIDOS, perdidos);
        values.put(Contratos.EstadisticaEntidad.SET1, setPartido1.getId());
        values.put(Contratos.EstadisticaEntidad.SET2, setPartido2.getId());
        values.put(Contratos.EstadisticaEntidad.SET3, setPartido3.getId());
        values.put(Contratos.EstadisticaEntidad.SET4, setPartido4.getId());
        values.put(Contratos.EstadisticaEntidad.SET5, setPartido5.getId());
        return values;
    }
}
