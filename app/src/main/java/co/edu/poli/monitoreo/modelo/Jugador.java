package co.edu.poli.monitoreo.modelo;

import android.content.ContentValues;

import java.util.List;
import java.util.UUID;

import co.edu.poli.monitoreo.persistencia.Contratos;

public class Jugador {
    private String id;
    private String nombres;
    private String apellidos;
    private String correo;
    private String edad;
    private String categoria;
    private String entrenador;
    private List<Partido> patidos;

    public Jugador(String nombres, String apellidos, String correo, String edad, String categoria, String entrenador) {
        this.entrenador = entrenador;
        this.id = UUID.randomUUID().toString();
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.edad = edad;
        this.categoria = categoria;
    }

    public Jugador(String id, String nombres, String apellidos, String correo, String edad, String categoria, String entrenador) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.edad = edad;
        this.categoria = categoria;
        this.entrenador = entrenador;
    }

    public String getId() {
        return id;
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public String getEdad() {
        return edad;
    }

    public String getCategoria() {
        return categoria;
    }

    public List<Partido> getPatidos() {
        return patidos;
    }

    public String getEntrenador() {
        return entrenador;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(Contratos.JugadorEntidad.ID, id);
        values.put(Contratos.JugadorEntidad.NOMBRES, nombres);
        values.put(Contratos.JugadorEntidad.APELLIDOS, apellidos);
        values.put(Contratos.JugadorEntidad.CORREO, correo);
        values.put(Contratos.JugadorEntidad.EDAD, edad);
        values.put(Contratos.JugadorEntidad.CATEGORIA, categoria);
        values.put(Contratos.JugadorEntidad.ENTRENADOR, entrenador);
        return values;
    }
}
