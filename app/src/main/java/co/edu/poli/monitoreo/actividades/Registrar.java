package co.edu.poli.monitoreo.actividades;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import co.edu.poli.monitoreo.R;

public class Registrar extends AppCompatActivity {

    private final String TAG = this.getClass().getName();
    private FirebaseAuth mAuth;

    private EditText edtCorreo;
    private EditText edtContrasena;
    private Button btnRegistar;
    private Button btnCancelar;
    public ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        edtCorreo = findViewById(R.id.edtCorreo);
        edtContrasena = findViewById(R.id.edtContrasena);
        btnRegistar = findViewById(R.id.btnRegistrar);
        btnCancelar = findViewById(R.id.btnCancelar);

        mAuth = FirebaseAuth.getInstance();


        btnRegistar.setOnClickListener(v -> registrar(edtCorreo.getText().toString(), edtContrasena.getText().toString()));

        btnCancelar.setOnClickListener(v -> finish());

    }

    private void registrar(String email, String password) {
        Log.d(TAG, "Registrando :" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "Entrenador registrado", Toast.LENGTH_LONG).show();
                        finish();
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "createUserWithEmail:success");
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        Toast.makeText(getApplicationContext(), "Error en los datos.",
                                Toast.LENGTH_SHORT).show();
                    }

                    // [START_EXCLUDE]
                    hideProgressDialog();
                    // [END_EXCLUDE]
                });
        // [END create_user_with_email]
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Cargando");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = edtCorreo.getText().toString();
        if (TextUtils.isEmpty(email)) {
            edtCorreo.setError("Requerido.");
            valid = false;
        } else {
            edtCorreo.setError(null);
        }

        String password = edtContrasena.getText().toString();
        if (TextUtils.isEmpty(password)) {
            edtContrasena.setError("Requerido.");
            valid = false;
        } else {
            edtContrasena.setError(null);
        }

        return valid;
    }
}
